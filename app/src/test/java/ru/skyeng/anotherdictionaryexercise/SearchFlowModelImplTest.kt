package ru.skyeng.anotherdictionaryexercise

import org.junit.Test
import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.searchflow.SearchFlowModel

class SearchFlowModelImplTest {

    val mockWord = Word(id = 0L, origin = "", translation = "", thumbnail = "")

    @Test
    fun `Search results initial state`() {
        val flowModel = SearchFlowModel.createDefault()
        val testObs = flowModel.selectedWord.test()
        testObs.assertValue { !it.isPresent }
    }

    @Test
    fun `Selection of word`() {
        val flowModel = SearchFlowModel.createDefault()
        val testObs = flowModel.selectedWord.test()
        flowModel.selectWord(mockWord)
        testObs.assertValueAt(testObs.valueCount() - 1) { it.require() === mockWord }
    }

    @Test
    fun `Deselection of word`() {
        val flowModel = SearchFlowModel.createDefault()
        val testObs = flowModel.selectedWord.test()
        flowModel.selectWord(mockWord)
        flowModel.deselectWord()
        testObs.assertValueAt(testObs.valueCount() - 1) { !it.isPresent }
    }
}