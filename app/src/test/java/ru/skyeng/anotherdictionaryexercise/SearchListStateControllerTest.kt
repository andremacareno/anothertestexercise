package ru.skyeng.anotherdictionaryexercise

import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Test
import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.repositories.WordsRepository
import ru.skyeng.anotherdictionaryexercise.searchlistcontroller.SearchListStateController
import java.util.concurrent.TimeUnit

class SearchListStateControllerTest {

    val mockWordsRepository = object : WordsRepository {

        override fun getWords(word: String): Single<List<Word>> = Single.never()
    }

    @Test
    fun `Initial state`() {
        val controller = SearchListStateController.createDefault(mockWordsRepository)
        val testObs = controller.listState.test()
        testObs.assertValue { it.data.isEmpty() && !it.loading && !it.error }
    }

    @Test
    fun `Entering keyword triggers loading`() {
        val scheduler = TestScheduler()
        val controller = SearchListStateController.createDefault(mockWordsRepository, scheduler)
        val testObs = controller.listState.test()
        controller.searchFor("a")
        scheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        testObs.assertValueAt(1) { it.loading }
    }

    @Test
    fun `Error changes state`() {
        val testScheduler = TestScheduler()
        val wordsRepository = object : WordsRepository {

            override fun getWords(word: String): Single<List<Word>> = Single.error(Exception("Test exception"))
        }
        val controller = SearchListStateController.createDefault(wordsRepository, testScheduler)
        val testObs = controller.listState.test()
        controller.searchFor("a")
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        testObs.assertValueAt(testObs.valueCount() - 1) { it.error }
    }

    @Test
    fun `Calling retry actually retries the query`() {
        var getWordsCallCount = 0
        var lastKeyword = ""
        val testScheduler = TestScheduler()
        val wordsRepository = object : WordsRepository {

            override fun getWords(word: String): Single<List<Word>> {
                getWordsCallCount++
                lastKeyword = word
                return Single.error(Exception("Test exception"))
            }
        }

        val keywordToSearch = "test"
        val controller = SearchListStateController.createDefault(wordsRepository, testScheduler)
        val testObs = controller.listState.test()
        controller.searchFor(keywordToSearch)
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        controller.retryLastSearch()
        assert(lastKeyword == keywordToSearch && getWordsCallCount == 2) { "Last keyword: $lastKeyword, getWordsCallCount: $getWordsCallCount"}
    }

    @Test
    fun `Search results actually represented in state`() {
        val word1 = Word(1L, "", "", "")
        val word2 = word1.copy(id = 2L)
        val word3 = word1.copy(id = 3L)

        val expectedList = listOf(word1, word2, word3)

        val wordsRepository = object : WordsRepository {

            override fun getWords(word: String): Single<List<Word>> {
                return Single.just(expectedList)
            }
        }
        val testScheduler = TestScheduler()
        val controller = SearchListStateController.createDefault(wordsRepository, testScheduler)
        val testObs = controller.listState.map { it.data }.observeOn(testScheduler).test()
        controller.searchFor("test")
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        testObs.assertValueAt(testObs.valueCount() - 1) { it == expectedList }
    }

    @Test
    fun `Empty search is ignored`() {

        var getWordsCalled = false
        val wordsRepository = object : WordsRepository {

            override fun getWords(word: String): Single<List<Word>> {
                getWordsCalled = true
                return Single.just(emptyList())
            }
        }
        val testScheduler = TestScheduler()
        val controller = SearchListStateController.createDefault(wordsRepository, testScheduler)
        val testObs = controller.listState.map { it.data }.observeOn(testScheduler).test()
        controller.searchFor("")
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        assert(!getWordsCalled)
    }

    @Test
    fun `Whitespace search is ignored`() {

        var getWordsCalled = false
        val wordsRepository = object : WordsRepository {

            override fun getWords(word: String): Single<List<Word>> {
                getWordsCalled = true
                return Single.just(emptyList())
            }
        }
        val testScheduler = TestScheduler()
        val controller = SearchListStateController.createDefault(wordsRepository, testScheduler)
        val testObs = controller.listState.map { it.data }.observeOn(testScheduler).test()
        controller.searchFor("     ")
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        assert(!getWordsCalled)
    }

    @Test
    fun `Error resets data`() {
        val testScheduler = TestScheduler()
        var callsCount = 0
        val wordsRepository = object : WordsRepository {

            override fun getWords(word: String): Single<List<Word>> {
                callsCount++
                if (callsCount == 2) {
                    return Single.error(Exception("Test exception"))
                } else {
                    return Single.just(listOf(Word(1L, "", "", "")))
                }
            }
        }
        val controller = SearchListStateController.createDefault(wordsRepository, testScheduler)
        val testObs = controller.listState.test()
        controller.searchFor("a")
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        testObs.assertValueAt(testObs.valueCount() - 1) { it.data.isNotEmpty() }
        controller.searchFor("error")
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        testObs.assertValueAt(testObs.valueCount() - 1) { it.data.isEmpty() }
    }

    @Test
    fun `Reset search results on empty query`() {
        val testScheduler = TestScheduler()
        val wordsRepository = object : WordsRepository {

            override fun getWords(word: String): Single<List<Word>> = Single.just(listOf(Word(1L, "", "", "")))
        }
        val controller = SearchListStateController.createDefault(wordsRepository, testScheduler)
        val testObs = controller.listState.test()
        controller.searchFor("a")
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        testObs.assertValueAt(testObs.valueCount() - 1) { it.data.isNotEmpty() }
        controller.searchFor("")
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        testObs.assertValueAt(testObs.valueCount() - 1) { it.data.isEmpty() }
    }

}