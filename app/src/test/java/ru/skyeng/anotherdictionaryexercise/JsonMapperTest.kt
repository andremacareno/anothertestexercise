package ru.skyeng.anotherdictionaryexercise

import org.junit.Test
import ru.skyeng.anotherdictionaryexercise.api.*
import ru.skyeng.anotherdictionaryexercise.json.JsonMapper

class JsonMapperTest {

    @Test
    fun `DefinitionApiModel from JSON`() {
        val json = "{\"text\":\"A single group of letters that are used together with a particular meaning.\",\"soundUrl\":\"//d2fmfepycn0xw0.cloudfront.net?gender=male&accent=british&text=a+single+group+of+letters+that+are+used+together+with+a+particular+meaning\"}"
        val deserializedModel = JsonMapper.deserialize<DefinitionApiModel>(json)
        val expectedText = "A single group of letters that are used together with a particular meaning."
        val expectedModel = DefinitionApiModel(expectedText)
        assert(deserializedModel == expectedModel)
    }

    @Test
    fun `ImageApiModel from JSON`() {
        val json = "{\"url\":\"//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=200&h=150&q=50\"}"
        val deserializedModel = JsonMapper.deserialize<ImageApiModel>(json)
        val expectedModel = ImageApiModel("//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=200&h=150&q=50")
        assert(deserializedModel == expectedModel)
    }

    @Test
    fun `UsageExampleApiModel from JSON`() {
        val json = "{\"text\":\"Some [words] are more difficult to spell than others.\",\"soundUrl\":\"//d2fmfepycn0xw0.cloudfront.net?gender=male&accent=british&text=Some+words+are+more+difficult+to+spell+than+others.\"}"
        val deserializedModel = JsonMapper.deserialize<UsageExampleApiModel>(json)
        val expectedModel = UsageExampleApiModel("Some [words] are more difficult to spell than others.")
        assert(deserializedModel == expectedModel)
    }

    @Test
    fun `TranslationApiModel from JSON`() {
        val json = "{\"text\":\"разговор\",\"note\":null}"
        val deserializedModel = JsonMapper.deserialize<TranslationApiModel>(json)
        val expectedModel = TranslationApiModel("разговор", null)
        assert(deserializedModel == expectedModel)
    }

    @Test
    fun `MeaningApiModel from JSON`() {
        val json = "{\"id\":\"92160\",\"wordId\":217,\"difficultyLevel\":1,\"partOfSpeechCode\":\"n\",\"prefix\":\"a\",\"text\":\"word\",\"soundUrl\":\"//d2fmfepycn0xw0.cloudfront.net?gender=male&accent=british&text=w%C9%9C%CB%90d&transcription=1\",\"transcription\":\"wɜːd\",\"properties\":{\"collocation\":false,\"countability\":\"c\",\"irregularPlural\":false,\"falseFriends\":[]},\"updatedAt\":\"2020-09-01 20:22:29\",\"mnemonics\":null,\"translation\":{\"text\":\"слово\",\"note\":null},\"images\":[{\"url\":\"//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=200&h=150&q=50\"}],\"definition\":{\"text\":\"A single group of letters that are used together with a particular meaning.\",\"soundUrl\":\"//d2fmfepycn0xw0.cloudfront.net?gender=male&accent=british&text=a+single+group+of+letters+that+are+used+together+with+a+particular+meaning\"},\"examples\":[{\"text\":\"Some [words] are more difficult to spell than others.\",\"soundUrl\":\"//d2fmfepycn0xw0.cloudfront.net?gender=male&accent=british&text=Some+words+are+more+difficult+to+spell+than+others.\"},{\"text\":\"Perhaps 'lucky' is not exactly the right [word].\",\"soundUrl\":\"//d2fmfepycn0xw0.cloudfront.net?gender=male&accent=british&text=Perhaps+%27lucky%27+is+not+exactly+the+right+word.\"}],\"meaningsWithSimilarTranslation\":[{\"meaningId\":92160,\"frequencyPercent\":\"90.3\",\"partOfSpeechAbbreviation\":\"сущ.\",\"translation\":{\"text\":\"слово\",\"note\":null}},{\"meaningId\":92162,\"frequencyPercent\":\"3.9\",\"partOfSpeechAbbreviation\":\"сущ.\",\"translation\":{\"text\":\"известие\",\"note\":null}},{\"meaningId\":92163,\"frequencyPercent\":\"2.3\",\"partOfSpeechAbbreviation\":\"сущ.\",\"translation\":{\"text\":\"приказ\",\"note\":\"\"}},{\"meaningId\":92164,\"frequencyPercent\":\"2.3\",\"partOfSpeechAbbreviation\":\"сущ.\",\"translation\":{\"text\":\"разговор\",\"note\":null}},{\"meaningId\":92165,\"frequencyPercent\":\"0.8\",\"partOfSpeechAbbreviation\":\"сущ.\",\"translation\":{\"text\":\"обещание\",\"note\":null}},{\"meaningId\":92170,\"frequencyPercent\":\"0.4\",\"partOfSpeechAbbreviation\":\"гл.\",\"translation\":{\"text\":\"выражать словами\",\"note\":null}}],\"alternativeTranslations\":[{\"text\":\"minute\",\"translation\":{\"text\":\"минута\",\"note\":null}},{\"text\":\"boy\",\"translation\":{\"text\":\"мальчик\",\"note\":null}},{\"text\":\"policy\",\"translation\":{\"text\":\"политика \",\"note\":null}},{\"text\":\"data\",\"translation\":{\"text\":\"данные\",\"note\":null}},{\"text\":\"email\",\"translation\":{\"text\":\"электронная почта\",\"note\":\"\"}},{\"text\":\"member\",\"translation\":{\"text\":\"член\",\"note\":\"организации, предприятия и т. д.\"}},{\"text\":\"son\",\"translation\":{\"text\":\"сын\",\"note\":null}},{\"text\":\"decade\",\"translation\":{\"text\":\"десятилетие\",\"note\":null}},{\"text\":\"month\",\"translation\":{\"text\":\"месяц\",\"note\":null}},{\"text\":\"office\",\"translation\":{\"text\":\"офис\",\"note\":null}},{\"text\":\"effect\",\"translation\":{\"text\":\"эффект\",\"note\":null}},{\"text\":\"hour\",\"translation\":{\"text\":\"час\",\"note\":null}},{\"text\":\"world\",\"translation\":{\"text\":\"жизнь\",\"note\":\"отдельного человека\"}},{\"text\":\"party\",\"translation\":{\"text\":\"партия\",\"note\":\"\"}},{\"text\":\"nation\",\"translation\":{\"text\":\"государство\",\"note\":null}},{\"text\":\"dog\",\"translation\":{\"text\":\"собака\",\"note\":null}},{\"text\":\"water\",\"translation\":{\"text\":\"вода\",\"note\":null}},{\"text\":\"day\",\"translation\":{\"text\":\"день\",\"note\":\"дата\"}},{\"text\":\"tax\",\"translation\":{\"text\":\"налог\",\"note\":null}},{\"text\":\"Moscow\",\"translation\":{\"text\":\"Москва\",\"note\":null}}]}"
        val deserializedModel = JsonMapper.deserialize<MeaningApiModel>(json)
        val expectedModel = MeaningApiModel(
            id = "92160",
            wordId = 217,
            partOfSpeechCode = "n",
            text = "word",
            transcription = "wɜːd",
            translation = TranslationApiModel("слово", null),
            images = listOf(ImageApiModel("//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=200&h=150&q=50")),
            definition = DefinitionApiModel("A single group of letters that are used together with a particular meaning."),
            examples = listOf(
                UsageExampleApiModel("Some [words] are more difficult to spell than others."),
                UsageExampleApiModel("Perhaps 'lucky' is not exactly the right [word].")
            )
        )
        assert(deserializedModel == expectedModel)
    }

    @Test
    fun `Meaning2ApiModel from JSON`() {
        val json = "{\"id\":92160,\"partOfSpeechCode\":\"n\",\"translation\":{\"text\":\"слово\",\"note\":null},\"previewUrl\":\"//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=96&h=72\",\"imageUrl\":\"//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=640&h=480\",\"transcription\":\"wɜːd\",\"soundUrl\":\"//d2fmfepycn0xw0.cloudfront.net?gender=male&accent=british&text=w%C9%9C%CB%90d&transcription=1\"}"
        val deserializedModel = JsonMapper.deserialize<Meaning2ApiModel>(json)
        val expectedModel = Meaning2ApiModel(
            id = 92160,
            partOfSpeechCode = "n",
            translation = TranslationApiModel("слово", note = null),
            previewUrl = "//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=96&h=72",
            imageUrl = "//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=640&h=480",
            transcription = "wɜːd"
        )
        assert(deserializedModel == expectedModel)
    }

    @Test
    fun `WordApiModel from JSON`() {
        val json = "{\"id\":217,\"text\":\"word\",\"meanings\":[{\"id\":92160,\"partOfSpeechCode\":\"n\",\"translation\":{\"text\":\"слово\",\"note\":null},\"previewUrl\":\"//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=96&h=72\",\"imageUrl\":\"//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=640&h=480\",\"transcription\":\"wɜːd\",\"soundUrl\":\"//d2fmfepycn0xw0.cloudfront.net?gender=male&accent=british&text=w%C9%9C%CB%90d&transcription=1\"},{\"id\":92162,\"partOfSpeechCode\":\"n\",\"translation\":{\"text\":\"известие\",\"note\":null},\"previewUrl\":\"//d2zkmv5t5kao9.cloudfront.net/images/d6e722ff3a3d143c40aad0bdb511c121.jpeg?w=96&h=72\",\"imageUrl\":\"//d2zkmv5t5kao9.cloudfront.net/images/d6e722ff3a3d143c40aad0bdb511c121.jpeg?w=640&h=480\",\"transcription\":\"wɜːd\",\"soundUrl\":\"//d2fmfepycn0xw0.cloudfront.net?gender=male&accent=british&text=word\"}]}"
        val deserializedModel = JsonMapper.deserialize<WordApiModel>(json)
        val expectedModel = WordApiModel(
            id = 217,
            text = "word",
            meanings = listOf(
                Meaning2ApiModel(
                    id = 92160,
                    partOfSpeechCode = "n",
                    translation = TranslationApiModel("слово", note = null),
                    previewUrl = "//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=96&h=72",
                    imageUrl = "//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=640&h=480",
                    transcription = "wɜːd"
                ),
                Meaning2ApiModel(
                    id = 92162,
                    partOfSpeechCode = "n",
                    translation = TranslationApiModel("известие", note = null),
                    previewUrl = "//d2zkmv5t5kao9.cloudfront.net/images/d6e722ff3a3d143c40aad0bdb511c121.jpeg?w=96&h=72",
                    imageUrl = "//d2zkmv5t5kao9.cloudfront.net/images/d6e722ff3a3d143c40aad0bdb511c121.jpeg?w=640&h=480",
                    transcription = "wɜːd"
                )
            )
        )
        assert(deserializedModel == expectedModel)
    }


}