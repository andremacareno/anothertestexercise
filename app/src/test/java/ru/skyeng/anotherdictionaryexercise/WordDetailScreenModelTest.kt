package ru.skyeng.anotherdictionaryexercise

import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Test
import ru.skyeng.anotherdictionaryexercise.detailscreen.WordDetailScreenModel
import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.models.WordDetail
import ru.skyeng.anotherdictionaryexercise.repositories.WordDetailsRepository
import ru.skyeng.anotherdictionaryexercise.utils.Optional
import ru.skyeng.anotherdictionaryexercise.wordselect.SelectedWordProvider
import ru.skyeng.anotherdictionaryexercise.wordselect.WordSelector

class WordDetailScreenModelTest {

    val mockWordDetailsRepository = object : WordDetailsRepository {

        override fun getDetails(id: Long): Single<WordDetail> = Single.never()
    }
    val mockWordProvider = object : SelectedWordProvider {

        override val selectedWord: Observable<Optional<Word>> = Observable.never()
    }
    val mockWordSelector = object : WordSelector {

        override fun selectWord(word: Word) = Unit
        override fun deselectWord() = Unit
    }

    val mockWord = Word(1L, "" ,"", "")
    val mockWordDetail = WordDetail(mockWord)

    @Test
    fun `Initial state`() {
        val screenModel = WordDetailScreenModel.createDefault(mockWordProvider, mockWordSelector, mockWordDetailsRepository)
        val testObs = screenModel.detailState.test()
        testObs.assertValue { it.loading && it.detail == null }
    }

    @Test
    fun `State after word selection`() {
        val wordRelay = PublishRelay.create<Word>()
        val wordProvider = object : SelectedWordProvider {

            override val selectedWord: Observable<Optional<Word>> = wordRelay.map { Optional.of(it) }
        }
        val wordDetailsRepository = object : WordDetailsRepository {

            override fun getDetails(id: Long): Single<WordDetail> = Single.just(mockWordDetail)
        }
        val screenModel = WordDetailScreenModel.createDefault(wordProvider, mockWordSelector, wordDetailsRepository)
        val testObs = screenModel.detailState.test()
        wordRelay.accept(mockWord)
        testObs.assertValueAt(testObs.valueCount() - 1) { !it.loading && it.detail === mockWordDetail }
    }

    @Test
    fun `State after unsuccessful request for details`() {
        val wordRelay = PublishRelay.create<Word>()
        val wordProvider = object : SelectedWordProvider {

            override val selectedWord: Observable<Optional<Word>> = wordRelay.map { Optional.of(it) }
        }
        val wordDetailsRepository = object : WordDetailsRepository {

            override fun getDetails(id: Long): Single<WordDetail> = Single.error(Exception("Test exception"))
        }
        val screenModel = WordDetailScreenModel.createDefault(wordProvider, mockWordSelector, wordDetailsRepository)
        val testObs = screenModel.detailState.test()
        wordRelay.accept(mockWord)
        testObs.assertValueAt(testObs.valueCount() - 1) { !it.loading && it.error && it.short === mockWord && it.detail == null }
    }

    @Test
    fun `Retrying actually executes new query`() {
        var getDetailsCallCount = 0
        val wordRelay = PublishRelay.create<Word>()
        val wordProvider = object : SelectedWordProvider {

            override val selectedWord: Observable<Optional<Word>> = wordRelay.map { Optional.of(it) }
        }
        val wordDetailsRepository = object : WordDetailsRepository {

            override fun getDetails(id: Long): Single<WordDetail> {
                getDetailsCallCount++
                return Single.error(IllegalArgumentException("test exception"))
            }
        }
        val screenModel = WordDetailScreenModel.createDefault(wordProvider, mockWordSelector, wordDetailsRepository)
        val testObs = screenModel.detailState.test()
        wordRelay.accept(mockWord)
        screenModel.retryDetailRequest()
        assert(getDetailsCallCount == 2)
    }
}