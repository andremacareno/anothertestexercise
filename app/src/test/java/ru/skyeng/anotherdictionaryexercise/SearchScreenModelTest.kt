package ru.skyeng.anotherdictionaryexercise

import org.junit.Test
import ru.skyeng.anotherdictionaryexercise.searchscreen.SearchScreenModel

class SearchScreenModelTest {

    @Test
    fun `Initial state`() {
        val searchScreenModel = SearchScreenModel.createDefault()
        val testObs = searchScreenModel.searchMode.test()
        testObs.assertValue(false)
    }

    @Test
    fun `Enabling search mode updates state`() {
        val searchScreenModel = SearchScreenModel.createDefault()
        val testObs = searchScreenModel.searchMode.test()
        searchScreenModel.enableSearchMode()
        testObs.assertValueAt(testObs.valueCount() - 1) { it == true }
    }

    @Test
    fun `Disabling search mode updates state`() {
        val searchScreenModel = SearchScreenModel.createDefault()
        val testObs = searchScreenModel.searchMode.test()
        searchScreenModel.enableSearchMode()
        searchScreenModel.disableSearchMode()
        testObs.assertValueAt(testObs.valueCount() - 1) { it == false }
    }
}