package ru.skyeng.anotherdictionaryexercise

import org.junit.Test
import ru.skyeng.anotherdictionaryexercise.api.Meaning2ApiModel
import ru.skyeng.anotherdictionaryexercise.api.TranslationApiModel
import ru.skyeng.anotherdictionaryexercise.api.WordApiModel
import ru.skyeng.anotherdictionaryexercise.api.WordApiToLocalDataMapper
import ru.skyeng.anotherdictionaryexercise.models.Word

class WordApiToLocalMapperTest {

    @Test
    fun `Mapping API model of Word to local one`() {
        val apiModel = WordApiModel(
            id = 217,
            text = "word",
            meanings = listOf(
                Meaning2ApiModel(
                    id = 92160,
                    partOfSpeechCode = "n",
                    translation = TranslationApiModel("слово", note = null),
                    previewUrl = "//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=96&h=72",
                    imageUrl = "//d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=640&h=480",
                    transcription = "wɜːd"
                ),
                Meaning2ApiModel(
                    id = 92162,
                    partOfSpeechCode = "n",
                    translation = TranslationApiModel("известие", note = null),
                    previewUrl = "//d2zkmv5t5kao9.cloudfront.net/images/d6e722ff3a3d143c40aad0bdb511c121.jpeg?w=96&h=72",
                    imageUrl = "//d2zkmv5t5kao9.cloudfront.net/images/d6e722ff3a3d143c40aad0bdb511c121.jpeg?w=640&h=480",
                    transcription = "wɜːd"
                )
            )
        )
        val expectedWords = listOf(
            Word(id = 92160, origin = "word", translation = "слово", thumbnail = "https://d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=96&h=72", photo = "https://d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=640&h=480"),
            Word(id = 92162, origin = "word", translation = "известие", thumbnail = "https://d2zkmv5t5kao9.cloudfront.net/images/d6e722ff3a3d143c40aad0bdb511c121.jpeg?w=96&h=72", photo = "https://d2zkmv5t5kao9.cloudfront.net/images/d6e722ff3a3d143c40aad0bdb511c121.jpeg?w=640&h=480")
        )

        val mappedWords = WordApiToLocalDataMapper.convert(apiModel)
        assert(expectedWords == mappedWords)
    }
}