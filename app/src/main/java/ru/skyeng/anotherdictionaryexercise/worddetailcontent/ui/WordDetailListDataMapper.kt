package ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui

import ru.skyeng.anotherdictionaryexercise.models.WordDetail

class WordDetailListDataMapper(private val strings: WordDetailListStrings) {

    fun mapData(input: WordDetail): List<WordDetailListItem> {
        return mutableListOf<WordDetailListItem>().apply {
            if (input.transcription.isNotEmpty()) {
                add(WordDetailListItem.Info(input.transcription))
            }
            if (input.definition.isNotEmpty()) {
                add(WordDetailListItem.Definition(input.definition))
            }
            add(WordDetailListItem.Header(strings.examplesHeader))
            if (input.examples.isEmpty()) {
                add(WordDetailListItem.UsageExample(strings.noExamples))
            } else {
                addAll(input.examples.map { WordDetailListItem.UsageExample(it.text) })
            }
        }
    }
}