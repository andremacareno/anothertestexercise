package ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui

interface WordDetailListStrings {

    val examplesHeader: String
    val noExamples: String
}