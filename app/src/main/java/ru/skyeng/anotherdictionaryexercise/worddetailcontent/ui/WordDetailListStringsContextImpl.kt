package ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui

import android.content.Context
import ru.skyeng.anotherdictionaryexercise.R

class WordDetailListStringsContextImpl(context: Context) : WordDetailListStrings {

    override val examplesHeader: String = context.getString(R.string.examples)

    override val noExamples: String = context.getString(R.string.no_examples)
}