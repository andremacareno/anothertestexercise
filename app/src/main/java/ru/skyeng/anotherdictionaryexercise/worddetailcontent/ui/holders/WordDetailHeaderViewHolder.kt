package ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.holders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.WordDetailListItem

class WordDetailHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), WordDetailItemViewHolder<WordDetailListItem.Header> {

    private val header: TextView = itemView as TextView

    override fun bind(item: WordDetailListItem.Header) {
        header.text = item.header
    }
}