package ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.holders

import ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.WordDetailListItem

interface WordDetailItemViewHolder<T : WordDetailListItem> {

    fun bind(item: T)
}