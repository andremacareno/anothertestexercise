package ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.holders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.skyeng.anotherdictionaryexercise.R
import ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.WordDetailListItem

class WordDetailDefinitionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), WordDetailItemViewHolder<WordDetailListItem.Definition> {

    private val definition: TextView = itemView.findViewById(R.id.definition)

    override fun bind(item: WordDetailListItem.Definition) {
        definition.text = item.definition
    }
}