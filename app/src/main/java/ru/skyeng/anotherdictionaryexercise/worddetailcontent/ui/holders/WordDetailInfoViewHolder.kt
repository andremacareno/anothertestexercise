package ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.holders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.skyeng.anotherdictionaryexercise.R
import ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.WordDetailListItem

class WordDetailInfoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), WordDetailItemViewHolder<WordDetailListItem.Info> {

    private val transcription: TextView = itemView.findViewById(R.id.transcription)

    override fun bind(item: WordDetailListItem.Info) {
        transcription.text = "/${item.transcription}/"
    }
}