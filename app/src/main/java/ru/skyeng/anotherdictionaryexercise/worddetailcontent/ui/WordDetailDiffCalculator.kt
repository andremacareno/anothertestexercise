package ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui

import androidx.recyclerview.widget.DiffUtil

class WordDetailDiffCalculator(
    private val oldData: List<WordDetailListItem>,
    private val newData: List<WordDetailListItem>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldData.size
    }

    override fun getNewListSize(): Int {
        return newData.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldData[oldItemPosition].isSameItem(newData[newItemPosition])
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldData[oldItemPosition].isSameContent(newData[newItemPosition])
    }
}