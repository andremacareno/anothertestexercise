package ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.skyeng.anotherdictionaryexercise.R
import ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.holders.*

class WordDetailListAdapter(private val data: List<WordDetailListItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private companion object {

        val itemTypeToLayoutId = mapOf(
            WordDetailListItem.ITEM_INFO to R.layout.item_detail_info,
            WordDetailListItem.ITEM_DEFINITION to R.layout.item_detail_definition,
            WordDetailListItem.ITEM_HEADER to R.layout.item_detail_header,
            WordDetailListItem.ITEM_USAGE_EXAMPLE to R.layout.item_detail_usage_example
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemViewType(position: Int): Int {
        return data[position].itemType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutId = itemTypeToLayoutId.getValue(viewType)
        val itemView = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        return when (viewType) {
            WordDetailListItem.ITEM_INFO -> WordDetailInfoViewHolder(itemView)
            WordDetailListItem.ITEM_DEFINITION -> WordDetailDefinitionViewHolder(itemView)
            WordDetailListItem.ITEM_HEADER -> WordDetailHeaderViewHolder(itemView)
            WordDetailListItem.ITEM_USAGE_EXAMPLE -> WordDetailUsageExampleViewHolder(itemView)
            else -> throw IllegalArgumentException("Unknown viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as WordDetailItemViewHolder<WordDetailListItem>).bind(data[position])
    }

}