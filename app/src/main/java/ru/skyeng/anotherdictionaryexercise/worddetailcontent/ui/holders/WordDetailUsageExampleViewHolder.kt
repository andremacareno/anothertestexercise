package ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.holders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.skyeng.anotherdictionaryexercise.R
import ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.WordDetailListItem

class WordDetailUsageExampleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), WordDetailItemViewHolder<WordDetailListItem.UsageExample> {

    private val example: TextView = itemView.findViewById(R.id.usage_example)

    override fun bind(item: WordDetailListItem.UsageExample) {
        example.text = item.example
    }
}