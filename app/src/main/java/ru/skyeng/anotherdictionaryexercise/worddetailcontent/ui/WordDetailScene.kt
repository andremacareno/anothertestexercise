package ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import ru.skyeng.anotherdictionaryexercise.models.WordDetail
import ru.skyeng.anotherdictionaryexercise.ui.AbsScene
import ru.skyeng.anotherdictionaryexercise.utils.bindAdapter
import ru.skyeng.anotherdictionaryexercise.worddetailcontent.WordDetailViewModel

class WordDetailScene(private val dependencies: Dependencies) : AbsScene<WordDetailViewModel>() {

    interface Dependencies {

        val recycler: RecyclerView
        val strings: WordDetailListStrings
    }

    private val currentListState = mutableListOf<WordDetailListItem>()
    private val adapter = WordDetailListAdapter(currentListState)
    private val dataMapper = WordDetailListDataMapper(dependencies.strings)

    override fun onBind(vm: WordDetailViewModel): Disposable {
        val ret = CompositeDisposable()
        ret += dependencies.recycler.bindAdapter(adapter)
        ret += vm.detail.observeOn(AndroidSchedulers.mainThread()).subscribe(::updateData)
        return ret
    }

    private fun updateData(newDetails: WordDetail) {
        val oldData = currentListState.toList()
        val newData = dataMapper.mapData(newDetails)
        val diffCalculator = WordDetailDiffCalculator(oldData, newData)
        val result = DiffUtil.calculateDiff(diffCalculator)
        currentListState.clear()
        currentListState.addAll(newData)
        result.dispatchUpdatesTo(adapter)
    }
}