package ru.skyeng.anotherdictionaryexercise.worddetailcontent

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.models.WordDetail

interface WordDetailProvider {

    val detail: Observable<WordDetail>
}