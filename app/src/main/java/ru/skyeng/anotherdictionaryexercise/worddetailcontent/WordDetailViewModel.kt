package ru.skyeng.anotherdictionaryexercise.worddetailcontent

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.models.WordDetail

interface WordDetailViewModel {

    val detail: Observable<WordDetail>

    companion object {

        fun createDefault(provider: WordDetailProvider): WordDetailViewModel = Impl(provider)
    }

    private class Impl(provider: WordDetailProvider) : WordDetailViewModel {

        override val detail: Observable<WordDetail> = provider.detail
    }
}