package ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui

sealed class WordDetailListItem(val itemType: Int) {

    companion object {

        const val ITEM_INFO = 1
        const val ITEM_DEFINITION = 2
        const val ITEM_HEADER = 3
        const val ITEM_USAGE_EXAMPLE = 4
    }

    abstract fun isSameItem(item: WordDetailListItem): Boolean
    abstract fun isSameContent(item: WordDetailListItem): Boolean

    class Info(val transcription: String) : WordDetailListItem(ITEM_INFO) {

        override fun isSameItem(item: WordDetailListItem): Boolean {
            return item is Info && item.transcription == transcription
        }

        override fun isSameContent(item: WordDetailListItem): Boolean = isSameItem(item)
    }

    class Definition(val definition: String) : WordDetailListItem(ITEM_DEFINITION) {

        override fun isSameItem(item: WordDetailListItem): Boolean {
            return item is Definition && item.definition == item.definition
        }

        override fun isSameContent(item: WordDetailListItem): Boolean = isSameItem(item)
    }

    class Header(val header: String) : WordDetailListItem(ITEM_HEADER) {

        override fun isSameItem(item: WordDetailListItem): Boolean {
            return item is Header && item.header == header
        }

        override fun isSameContent(item: WordDetailListItem): Boolean = isSameItem(item)
    }

    class UsageExample(val example: String) : WordDetailListItem(ITEM_USAGE_EXAMPLE) {

        override fun isSameItem(item: WordDetailListItem): Boolean {
            return item is UsageExample && item.example == item.example
        }

        override fun isSameContent(item: WordDetailListItem): Boolean = isSameItem(item)
    }
}