package ru.skyeng.anotherdictionaryexercise

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.skyeng.anotherdictionaryexercise.fragments.SearchFlowFragment

class MainActivity : AppCompatActivity() {

    private var beginFlow = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        beginFlow = savedInstanceState == null
    }

    override fun onResume() {
        super.onResume()
        if (beginFlow) {
            beginFlow = false
            supportFragmentManager.beginTransaction().add(R.id.flow_container, SearchFlowFragment()).commit()
        }
    }
}