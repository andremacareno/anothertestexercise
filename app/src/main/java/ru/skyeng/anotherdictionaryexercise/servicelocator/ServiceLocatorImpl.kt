package ru.skyeng.anotherdictionaryexercise.servicelocator

class ServiceLocatorImpl(
    private val typeToService: Map<Class<*>, Lazy<*>>
) : ServiceLocator {

    override fun <T> get(type: Class<T>): T {
        return typeToService.getValue(type).value!! as T
    }
}