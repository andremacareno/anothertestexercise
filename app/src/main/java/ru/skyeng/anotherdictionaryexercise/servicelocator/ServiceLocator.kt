package ru.skyeng.anotherdictionaryexercise.servicelocator

interface ServiceLocator {

    fun <T> get(type: Class<T>): T
}

inline fun <reified T> ServiceLocator.get(): T {
    return get(T::class.java)
}