package ru.skyeng.anotherdictionaryexercise.detailscreen

import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.models.WordDetail

data class WordDetailState(
    val loading: Boolean,
    val error: Boolean,
    val short: Word?,
    val detail: WordDetail?
)