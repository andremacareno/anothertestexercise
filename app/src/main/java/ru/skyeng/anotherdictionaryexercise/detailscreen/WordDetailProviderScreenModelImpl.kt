package ru.skyeng.anotherdictionaryexercise.detailscreen

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.models.WordDetail
import ru.skyeng.anotherdictionaryexercise.worddetailcontent.WordDetailProvider

class WordDetailProviderScreenModelImpl(screenModel: WordDetailScreenModel) : WordDetailProvider {

    override val detail: Observable<WordDetail> = screenModel.detailState.filter { !it.loading && it.detail != null }.map { it.detail!! }
}