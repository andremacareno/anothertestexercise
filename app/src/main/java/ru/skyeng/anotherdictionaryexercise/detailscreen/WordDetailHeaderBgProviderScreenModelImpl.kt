package ru.skyeng.anotherdictionaryexercise.detailscreen

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.worddetailheader.WordDetailHeaderBgProvider

class WordDetailHeaderBgProviderScreenModelImpl(screenModel: WordDetailScreenModel) :
    WordDetailHeaderBgProvider {

    override val backgroundImage: Observable<String> = screenModel
        .detailState
        .filter { it.short != null && it.short.photo.isNotEmpty() }.map { it.short!!.photo }.distinctUntilChanged()
}