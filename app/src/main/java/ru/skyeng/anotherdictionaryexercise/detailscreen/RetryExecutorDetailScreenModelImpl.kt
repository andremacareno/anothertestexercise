package ru.skyeng.anotherdictionaryexercise.detailscreen

import ru.skyeng.anotherdictionaryexercise.retrytrigger.RetryExecutor

class RetryExecutorDetailScreenModelImpl(private val screenModel: WordDetailScreenModel) :
    RetryExecutor {

    override fun retry() = screenModel.retryDetailRequest()
}