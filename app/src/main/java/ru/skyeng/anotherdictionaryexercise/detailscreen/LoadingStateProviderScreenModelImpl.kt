package ru.skyeng.anotherdictionaryexercise.detailscreen

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.loadingindicator.LoadingStateProvider

class LoadingStateProviderScreenModelImpl(screenModel: WordDetailScreenModel) :
    LoadingStateProvider {

    override val loading: Observable<Boolean> = screenModel.detailState.map { it.loading }
}