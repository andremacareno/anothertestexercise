package ru.skyeng.anotherdictionaryexercise.detailscreen

import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.models.WordDetail

sealed class WordDetailStateEffect {

    abstract fun applyEffect(state: WordDetailState): WordDetailState

    class ReceivedShort(private val short: Word): WordDetailStateEffect() {

        override fun applyEffect(state: WordDetailState): WordDetailState = state.copy(short = short)
    }

    class ReceivedDetails(private val detail: WordDetail): WordDetailStateEffect() {

        override fun applyEffect(state: WordDetailState): WordDetailState = state.copy(loading = false, error = false, detail = detail)

    }

    object Error : WordDetailStateEffect() {

        override fun applyEffect(state: WordDetailState): WordDetailState = state.copy(loading = false, error = true, detail = null)
    }

    object Loading : WordDetailStateEffect() {

        override fun applyEffect(state: WordDetailState): WordDetailState = state.copy(loading = true, error = false, detail = null)
    }

}