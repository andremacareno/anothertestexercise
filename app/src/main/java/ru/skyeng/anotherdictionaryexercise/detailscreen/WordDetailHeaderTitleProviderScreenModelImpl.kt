package ru.skyeng.anotherdictionaryexercise.detailscreen

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.worddetailheader.WordDetailHeaderTitleProvider

class WordDetailHeaderTitleProviderScreenModelImpl(screenModel: WordDetailScreenModel) :
    WordDetailHeaderTitleProvider {

    override val title: Observable<String> = screenModel.word.map { it.origin }
    override val translation: Observable<String> = screenModel.word.map { it.translation }
}