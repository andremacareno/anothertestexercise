package ru.skyeng.anotherdictionaryexercise.detailscreen

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.retrytrigger.ErrorStateProvider

class ErrorStateProviderScreenModelImpl(screenModel: WordDetailScreenModel) : ErrorStateProvider {

    override val error: Observable<Boolean> = screenModel.detailState.map { !it.loading && it.detail == null }
}