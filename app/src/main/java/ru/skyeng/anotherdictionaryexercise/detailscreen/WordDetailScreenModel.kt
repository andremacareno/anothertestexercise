package ru.skyeng.anotherdictionaryexercise.detailscreen

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.rxkotlin.withLatestFrom
import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.repositories.WordDetailsRepository
import ru.skyeng.anotherdictionaryexercise.wordselect.SelectedWordProvider
import ru.skyeng.anotherdictionaryexercise.wordselect.WordSelector

interface WordDetailScreenModel {

    val word: Observable<Word>
    val detailState: Observable<WordDetailState>

    fun retryDetailRequest()
    fun notifyWordDeselected()

    companion object {

        fun createDefault(
            selectedWordProvider: SelectedWordProvider,
            wordSelector: WordSelector,
            detailsRepository: WordDetailsRepository
        ): WordDetailScreenModel = Impl(selectedWordProvider, wordSelector, detailsRepository)
    }

    private class Impl(
        selectedWordProvider: SelectedWordProvider,
        private val wordSelector: WordSelector,
        private val detailsRepository: WordDetailsRepository
    ): WordDetailScreenModel {

//        override val word: Observable<Word> =


        private val selectedWord = selectedWordProvider.selectedWord.filter { it.isPresent }.map { it.require() }
        private val retryTrigger = PublishRelay.create<Unit>()

        private val short = Observable.merge(
            selectedWord,
            retryTrigger.withLatestFrom(selectedWord) { _, word -> word }
        )

        private val shortReceived = short.map { WordDetailStateEffect.ReceivedShort(it) }
        private val loadingBegan = PublishRelay.create<WordDetailStateEffect.Loading>()

        private val detailedDefinition = BehaviorRelay.create<WordDetailStateEffect>().apply {
            short
                .doOnNext { loadingBegan.accept(WordDetailStateEffect.Loading) }
                .switchMap {
                    detailsRepository.getDetails(it.id)
                        .map<WordDetailStateEffect> { WordDetailStateEffect.ReceivedDetails(it) }
                        .onErrorReturn { WordDetailStateEffect.Error }
                        .toObservable()
                }
                .subscribe(this)
        }

        override val detailState: Observable<WordDetailState> = BehaviorRelay.create<WordDetailState>().apply {
            Observable
                .merge(
                    shortReceived,
                    loadingBegan,
                    detailedDefinition
                )
                .scan(WordDetailState(true, false, null, null)) { state, effect -> effect.applyEffect(state) }
                .subscribe(this)
        }

        override val word: Observable<Word> = detailState.filter { it.short != null }.map { it.short!! }

        override fun retryDetailRequest() = retryTrigger.accept(Unit)

        override fun notifyWordDeselected() = wordSelector.deselectWord()
    }
}