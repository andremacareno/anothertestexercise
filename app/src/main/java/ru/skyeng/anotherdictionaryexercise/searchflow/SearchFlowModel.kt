package ru.skyeng.anotherdictionaryexercise.searchflow

import com.jakewharton.rxrelay2.BehaviorRelay
import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.utils.Optional
import ru.skyeng.anotherdictionaryexercise.wordselect.SelectedWordProvider
import ru.skyeng.anotherdictionaryexercise.wordselect.WordSelector

interface SearchFlowModel : SelectedWordProvider, WordSelector {

    companion object {

        fun createDefault() : SearchFlowModel = Impl()
    }

    private class Impl : SearchFlowModel {

        override val selectedWord = BehaviorRelay.createDefault<Optional<Word>>(Optional.empty())

        override fun selectWord(word: Word) = selectedWord.accept(Optional.of(word))

        override fun deselectWord() = selectedWord.accept(Optional.empty())

    }
}