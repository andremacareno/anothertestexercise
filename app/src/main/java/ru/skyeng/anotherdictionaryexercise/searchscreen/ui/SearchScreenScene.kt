package ru.skyeng.anotherdictionaryexercise.searchscreen.ui

import android.view.View
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import ru.skyeng.anotherdictionaryexercise.searchscreen.SearchScreenModel
import ru.skyeng.anotherdictionaryexercise.ui.AbsScene

class SearchScreenScene(private val dependencies: Dependencies) : AbsScene<SearchScreenModel>() {

    interface Dependencies {

        val searchField: View
    }

    override fun onBind(vm: SearchScreenModel): Disposable {
        return Disposables.empty()
    }

}