package ru.skyeng.anotherdictionaryexercise.searchscreen

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.retrytrigger.ErrorStateProvider
import ru.skyeng.anotherdictionaryexercise.searchlistcontroller.SearchListStateController

class ErrorStateProviderListControllerImpl(val controller: SearchListStateController) :
    ErrorStateProvider {

    override val error: Observable<Boolean> = controller.listState.map { it.error }
}