package ru.skyeng.anotherdictionaryexercise.searchscreen

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.condview.ConditionallyVisibleViewStateProvider
import ru.skyeng.anotherdictionaryexercise.searchlistcontroller.SearchListStateController

class NoResultsViewStateProviderListControllerImpl(private val listStateController: SearchListStateController) : ConditionallyVisibleViewStateProvider {

    override val visiblility: Observable<Boolean> = listStateController.listState.map { it.query.isNotEmpty() && !it.error && !it.loading && it.data.isEmpty() }.distinctUntilChanged()
}