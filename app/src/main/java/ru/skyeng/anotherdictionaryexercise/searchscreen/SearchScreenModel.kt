package ru.skyeng.anotherdictionaryexercise.searchscreen

import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable

interface SearchScreenModel {

    val searchMode: Observable<Boolean>

    fun enableSearchMode()
    fun disableSearchMode()

    companion object {

        fun createDefault(): SearchScreenModel = Impl()
    }

    private class Impl() : SearchScreenModel {

        private val _searchMode = BehaviorRelay.createDefault(false)
        override val searchMode: Observable<Boolean> = _searchMode.distinctUntilChanged()

        override fun enableSearchMode() = _searchMode.accept(true)
        override fun disableSearchMode() = _searchMode.accept(false)
    }
}