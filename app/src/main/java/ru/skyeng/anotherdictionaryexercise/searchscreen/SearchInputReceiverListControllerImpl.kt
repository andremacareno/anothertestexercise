package ru.skyeng.anotherdictionaryexercise.searchscreen

import ru.skyeng.anotherdictionaryexercise.searchfield.SearchInputReceiver
import ru.skyeng.anotherdictionaryexercise.searchlistcontroller.SearchListStateController

class SearchInputReceiverListControllerImpl(private val controller: SearchListStateController) :
    SearchInputReceiver {

    override fun search(keyword: String) = controller.searchFor(keyword)
}