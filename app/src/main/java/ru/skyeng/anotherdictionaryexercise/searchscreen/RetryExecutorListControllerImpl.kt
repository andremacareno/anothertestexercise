package ru.skyeng.anotherdictionaryexercise.searchscreen

import ru.skyeng.anotherdictionaryexercise.retrytrigger.RetryExecutor
import ru.skyeng.anotherdictionaryexercise.searchlistcontroller.SearchListStateController

class RetryExecutorListControllerImpl(private val controller: SearchListStateController) :
    RetryExecutor {

    override fun retry() = controller.retryLastSearch()
}