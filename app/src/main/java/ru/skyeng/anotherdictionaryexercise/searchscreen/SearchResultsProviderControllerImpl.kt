package ru.skyeng.anotherdictionaryexercise.searchscreen

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.searchlistcontroller.SearchListStateController
import ru.skyeng.anotherdictionaryexercise.searchresults.SearchResultsProvider

class SearchResultsProviderControllerImpl(private val controller: SearchListStateController) :
    SearchResultsProvider {

    override val searchResults: Observable<List<Word>> = controller.listState.map { it.data }
}