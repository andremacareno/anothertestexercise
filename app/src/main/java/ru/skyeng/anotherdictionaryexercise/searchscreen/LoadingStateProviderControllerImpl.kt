package ru.skyeng.anotherdictionaryexercise.searchscreen

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.loadingindicator.LoadingStateProvider
import ru.skyeng.anotherdictionaryexercise.searchlistcontroller.SearchListStateController

class LoadingStateProviderControllerImpl(private val controller: SearchListStateController) :
    LoadingStateProvider {

    override val loading: Observable<Boolean> = controller.listState.map { it.loading }
}