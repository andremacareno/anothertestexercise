package ru.skyeng.anotherdictionaryexercise.fragments

import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.ViewModel
import ru.skyeng.anotherdictionaryexercise.ExerciseApp
import ru.skyeng.anotherdictionaryexercise.R
import ru.skyeng.anotherdictionaryexercise.repositories.WordDetailsRepository
import ru.skyeng.anotherdictionaryexercise.repositories.WordsRepository
import ru.skyeng.anotherdictionaryexercise.routing.SearchFlowRouterImpl
import ru.skyeng.anotherdictionaryexercise.searchflow.SearchFlowModel
import ru.skyeng.anotherdictionaryexercise.servicelocator.get
import ru.skyeng.anotherdictionaryexercise.utils.PostponeUntilResumeExecutor

class SearchFlowFragment : Fragment(R.layout.fragment_container) {

    class LifecycleStorage : ViewModel() {

        val flowModel by lazy(LazyThreadSafetyMode.NONE) { SearchFlowModel.createDefault() }
    }

    private val backStackListener: FragmentManager.OnBackStackChangedListener by lazy(LazyThreadSafetyMode.NONE) {
        FragmentManager.OnBackStackChangedListener { backPressedCallback.isEnabled = childFragmentManager.backStackEntryCount > 0 }
    }

    private val backPressedCallback: OnBackPressedCallback by lazy(LazyThreadSafetyMode.NONE) {
        object : OnBackPressedCallback(childFragmentManager.backStackEntryCount > 0) {

            override fun handleOnBackPressed() {
                router.goBack()
            }
        }
    }

    private val lifecycleObserver: LifecycleEventObserver by lazy(LazyThreadSafetyMode.NONE) {

        LifecycleEventObserver { _, event ->
            if (event === Lifecycle.Event.ON_PAUSE) {
                postponeUntilResumeExecutor.notifyPaused()
            } else if (event === Lifecycle.Event.ON_RESUME) {
                postponeUntilResumeExecutor.notifyResumed()
            }
        }
    }

    private val postponeUntilResumeExecutor by lazy(LazyThreadSafetyMode.NONE) { PostponeUntilResumeExecutor() }

    private val wordsRepository by lazy(LazyThreadSafetyMode.NONE) { ExerciseApp.sharedInstance.serviceLocator.get<WordsRepository>() }
    private val wordDetailsRepository by lazy(LazyThreadSafetyMode.NONE) { ExerciseApp.sharedInstance.serviceLocator.get<WordDetailsRepository>() }
    private val router by lazy(LazyThreadSafetyMode.NONE) { SearchFlowRouterImpl(R.id.container, childFragmentManager, postponeUntilResumeExecutor) }
    private val lifecycleStorage: LifecycleStorage by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        childFragmentManager.fragmentFactory = SearchFlowFragmentFactory(wordsRepository, wordDetailsRepository, lifecycleStorage.flowModel, router)
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            router.start()
        }
        requireActivity().onBackPressedDispatcher.addCallback(backPressedCallback)
        requireActivity().lifecycle.addObserver(lifecycleObserver)
        childFragmentManager.addOnBackStackChangedListener(backStackListener)
    }

    override fun onDestroy() {
        childFragmentManager.removeOnBackStackChangedListener(backStackListener)
        requireActivity().lifecycle.removeObserver(lifecycleObserver)
        super.onDestroy()
    }
}