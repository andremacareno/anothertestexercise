package ru.skyeng.anotherdictionaryexercise.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import ru.skyeng.anotherdictionaryexercise.R
import ru.skyeng.anotherdictionaryexercise.condview.ConditionallyVisibleViewModel
import ru.skyeng.anotherdictionaryexercise.condview.ui.ConditionallyVisibleScene
import ru.skyeng.anotherdictionaryexercise.loadingindicator.LoadingIndicatorViewModel
import ru.skyeng.anotherdictionaryexercise.loadingindicator.ui.LoadingIndicatorScene
import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.repositories.WordsRepository
import ru.skyeng.anotherdictionaryexercise.retrytrigger.RetryRequestViewModel
import ru.skyeng.anotherdictionaryexercise.retrytrigger.ui.RetryRequestScene
import ru.skyeng.anotherdictionaryexercise.routing.SearchFlowRouter
import ru.skyeng.anotherdictionaryexercise.searchfield.SearchFieldViewModel
import ru.skyeng.anotherdictionaryexercise.searchfield.ui.SearchFieldScene
import ru.skyeng.anotherdictionaryexercise.searchlistcontroller.SearchListStateController
import ru.skyeng.anotherdictionaryexercise.searchresults.SearchResultsListViewModel
import ru.skyeng.anotherdictionaryexercise.searchresults.ui.SearchResultsScene
import ru.skyeng.anotherdictionaryexercise.searchscreen.*
import ru.skyeng.anotherdictionaryexercise.searchscreen.ui.SearchScreenScene
import ru.skyeng.anotherdictionaryexercise.ui.BackPressHandlingEditText
import ru.skyeng.anotherdictionaryexercise.utils.hideKeyboard
import ru.skyeng.anotherdictionaryexercise.wordselect.WordSelector

class SearchFragment(
    private val wordsRepository: WordsRepository,
    private val wordSelector: WordSelector,
    private val router: SearchFlowRouter
) : Fragment(R.layout.fragment_search) {

    class LifecycleStorage(wordsRepository: WordsRepository, wordSelector: WordSelector) : ViewModel() {

        val screenModel by lazy(LazyThreadSafetyMode.NONE) { SearchScreenModel.createDefault() }

        val listStateController by lazy(LazyThreadSafetyMode.NONE) { SearchListStateController.createDefault(wordsRepository) }

        private val searchInputReceiver by lazy(LazyThreadSafetyMode.NONE) { SearchInputReceiverListControllerImpl(listStateController) }
        private val searchResultsProvider by lazy(LazyThreadSafetyMode.NONE) { SearchResultsProviderControllerImpl(listStateController) }
        private val loadingStateProvider by lazy(LazyThreadSafetyMode.NONE) { LoadingStateProviderControllerImpl(listStateController) }
        private val errorStateProvider by lazy(LazyThreadSafetyMode.NONE) { ErrorStateProviderListControllerImpl(listStateController) }
        private val retryExecutor by lazy(LazyThreadSafetyMode.NONE) { RetryExecutorListControllerImpl(listStateController) }
        private val noResultsViewStateProvider by lazy(LazyThreadSafetyMode.NONE) { NoResultsViewStateProviderListControllerImpl(listStateController) }
        private val emptyQueryViewStateProvider by lazy(LazyThreadSafetyMode.NONE) { EmptyQueryViewStateProviderListControllerImpl(listStateController) }

        val searchViewModel by lazy(LazyThreadSafetyMode.NONE) { SearchFieldViewModel.createDefault(searchInputReceiver) }
        val searchResultsListViewModel by lazy(LazyThreadSafetyMode.NONE) { SearchResultsListViewModel.createDefault(searchResultsProvider, wordSelector) }
        val loadingIndicatorViewModel by lazy(LazyThreadSafetyMode.NONE) { LoadingIndicatorViewModel.createDefault(loadingStateProvider) }
        val errorViewModel by lazy(LazyThreadSafetyMode.NONE) { RetryRequestViewModel.createDefault(errorStateProvider, retryExecutor) }
        val noResultsViewModel by lazy(LazyThreadSafetyMode.NONE) { ConditionallyVisibleViewModel.createDefault(noResultsViewStateProvider) }
        val emptyQueryViewModel by lazy(LazyThreadSafetyMode.NONE) { ConditionallyVisibleViewModel.createDefault(emptyQueryViewStateProvider) }
    }

    private val lifecycleStorage: LifecycleStorage by activityViewModels {

        object : ViewModelProvider.Factory {

            override fun <T : ViewModel?> create(modelClass: Class<T>): T = LifecycleStorage(wordsRepository, wordSelector) as T

        }
    }

    private val subscriptions = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        initUI(view!!)
        return view
    }

    private fun initUI(view: View) {
        val searchFieldView = view.findViewById<BackPressHandlingEditText>(R.id.search)
        val clearSearchView = view.findViewById<View>(R.id.clear_search)
        val recyclerView = view.findViewById<RecyclerView>(R.id.search_results).apply {
            itemAnimator = null
            addItemDecoration(DividerItemDecoration(context, RecyclerView.VERTICAL))
        }
        val errorContainer = view.findViewById<View>(R.id.error_container)
        val retryButton = errorContainer.findViewById<View>(R.id.retry_button)
        val loadingIndicator = view.findViewById<View>(R.id.loading_indicator)

        val noResultsView = view.findViewById<View>(R.id.no_results)
        val emptyQueryView = view.findViewById<View>(R.id.empty_query)

        val searchSceneDependencies = object : SearchFieldScene.Dependencies {

            override val searchField: BackPressHandlingEditText = searchFieldView
            override val clear: View = clearSearchView
        }

        val searchResultsSceneDependencies = object : SearchResultsScene.Dependencies {

            override val recycler: RecyclerView = recyclerView
            override val itemSelectionHandler: (Word) -> Unit = {
                searchFieldView.hideKeyboard()
                router.openWordDetail(it)
            }
        }

        val loadingIndicatorSceneDependencies = object : LoadingIndicatorScene.Dependencies {

            override val indicator: View = loadingIndicator
        }

        val screenSceneDependencies = object : SearchScreenScene.Dependencies {

            override val searchField: View = searchFieldView
        }

        val errorContainerDependencies = object : RetryRequestScene.Dependencies {

            override val retryContainer: View = errorContainer
            override val retryButton: View = retryButton
        }

        val noResultsDependencies = object : ConditionallyVisibleScene.Dependencies {

            override val conditionallyVisibleView: View = noResultsView
        }

        val emptyQueryDependencies = object : ConditionallyVisibleScene.Dependencies {

            override val conditionallyVisibleView: View = emptyQueryView
        }

        val searchScene = SearchFieldScene(searchSceneDependencies)
        val searchResultsScene = SearchResultsScene(searchResultsSceneDependencies)
        val indicatorScene = LoadingIndicatorScene(loadingIndicatorSceneDependencies)
        val screenScene = SearchScreenScene(screenSceneDependencies)
        val errorContainerScene = RetryRequestScene(errorContainerDependencies)
        val noResultsScene = ConditionallyVisibleScene(noResultsDependencies)
        val emptyQueryScene = ConditionallyVisibleScene(emptyQueryDependencies)

        subscriptions += searchScene.bind(lifecycleStorage.searchViewModel)
        subscriptions += searchResultsScene.bind(lifecycleStorage.searchResultsListViewModel)
        subscriptions += indicatorScene.bind(lifecycleStorage.loadingIndicatorViewModel)
        subscriptions += screenScene.bind(lifecycleStorage.screenModel)
        subscriptions += errorContainerScene.bind(lifecycleStorage.errorViewModel)
        subscriptions += noResultsScene.bind(lifecycleStorage.noResultsViewModel)
        subscriptions += emptyQueryScene.bind(lifecycleStorage.emptyQueryViewModel)
    }


    override fun onDestroy() {
        subscriptions.dispose()
        super.onDestroy()
    }

}