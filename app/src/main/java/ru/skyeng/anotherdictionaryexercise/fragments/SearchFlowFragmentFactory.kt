package ru.skyeng.anotherdictionaryexercise.fragments

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import ru.skyeng.anotherdictionaryexercise.repositories.WordDetailsRepository
import ru.skyeng.anotherdictionaryexercise.repositories.WordsRepository
import ru.skyeng.anotherdictionaryexercise.routing.SearchFlowRouter
import ru.skyeng.anotherdictionaryexercise.searchflow.SearchFlowModel

class SearchFlowFragmentFactory(
    private val wordsRepository: WordsRepository,
    private val wordDetailsRepository: WordDetailsRepository,
    private val searchFlowModel: SearchFlowModel,
    private val router: SearchFlowRouter
) : FragmentFactory() {

    override fun instantiate(classLoader: ClassLoader, className: String): Fragment = when (className) {
        SearchFragment::class.java.name -> SearchFragment(wordsRepository, searchFlowModel, router)
        WordDetailFragment::class.java.name -> WordDetailFragment(searchFlowModel, searchFlowModel, wordDetailsRepository)
        else -> super.instantiate(classLoader, className)
    }
}