package ru.skyeng.anotherdictionaryexercise.fragments

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import ru.skyeng.anotherdictionaryexercise.R
import ru.skyeng.anotherdictionaryexercise.detailscreen.*
import ru.skyeng.anotherdictionaryexercise.loadingindicator.LoadingIndicatorViewModel
import ru.skyeng.anotherdictionaryexercise.loadingindicator.ui.LoadingIndicatorScene
import ru.skyeng.anotherdictionaryexercise.repositories.WordDetailsRepository
import ru.skyeng.anotherdictionaryexercise.retrytrigger.RetryRequestViewModel
import ru.skyeng.anotherdictionaryexercise.retrytrigger.ui.RetryRequestScene
import ru.skyeng.anotherdictionaryexercise.worddetailcontent.WordDetailViewModel
import ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.WordDetailListStrings
import ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.WordDetailListStringsContextImpl
import ru.skyeng.anotherdictionaryexercise.worddetailcontent.ui.WordDetailScene
import ru.skyeng.anotherdictionaryexercise.worddetailheader.WordDetailHeaderScene
import ru.skyeng.anotherdictionaryexercise.worddetailheader.WordDetailHeaderViewModel
import ru.skyeng.anotherdictionaryexercise.wordselect.SelectedWordProvider
import ru.skyeng.anotherdictionaryexercise.wordselect.WordSelector

class WordDetailFragment(
    private val selectedWordProvider: SelectedWordProvider,
    private val wordSelector: WordSelector,
    private val wordDetailsRepository: WordDetailsRepository
) : Fragment(R.layout.fragment_detail) {

    class LifecycleStorage(
        selectedWordProvider: SelectedWordProvider,
        wordSelector: WordSelector,
        wordDetailsRepository: WordDetailsRepository
    ) : ViewModel() {

        private val screenModel by lazy(LazyThreadSafetyMode.NONE) { WordDetailScreenModel.createDefault(selectedWordProvider, wordSelector, wordDetailsRepository) }
        private val titleProvider by lazy(LazyThreadSafetyMode.NONE) { WordDetailHeaderTitleProviderScreenModelImpl(screenModel) }
        private val bgImageProvider by lazy(LazyThreadSafetyMode.NONE) { WordDetailHeaderBgProviderScreenModelImpl(screenModel) }
        private val wordDetailProvider by lazy(LazyThreadSafetyMode.NONE) { WordDetailProviderScreenModelImpl(screenModel) }
        private val loadingStateProvider by lazy(LazyThreadSafetyMode.NONE) { LoadingStateProviderScreenModelImpl(screenModel) }
        private val errorStateProvider by lazy(LazyThreadSafetyMode.NONE) { ErrorStateProviderScreenModelImpl(screenModel) }
        private val retryExecutor by lazy(LazyThreadSafetyMode.NONE) { RetryExecutorDetailScreenModelImpl(screenModel) }

        val headerViewModel by lazy(LazyThreadSafetyMode.NONE) { WordDetailHeaderViewModel.createDefault(titleProvider, bgImageProvider) }
        val detailsViewModel by lazy(LazyThreadSafetyMode.NONE) { WordDetailViewModel.createDefault(wordDetailProvider) }
        val loadingIndicatorViewModel by lazy(LazyThreadSafetyMode.NONE) { LoadingIndicatorViewModel.createDefault(loadingStateProvider) }
        val errorViewModel by lazy(LazyThreadSafetyMode.NONE) { RetryRequestViewModel.createDefault(errorStateProvider, retryExecutor) }
    }

    private val stringsProvider: WordDetailListStrings by lazy(LazyThreadSafetyMode.NONE) { WordDetailListStringsContextImpl(requireContext()) }
    private val lifecycleStorage: LifecycleStorage by activityViewModels { object : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T = LifecycleStorage(selectedWordProvider, wordSelector, wordDetailsRepository) as T
    } }

    private val subscriptions = CompositeDisposable()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val wordImageView = view.findViewById<ImageView>(R.id.image)
        val wordTitleTextView = view.findViewById<TextView>(R.id.word)
        val wordTranslationTextView = view.findViewById<TextView>(R.id.translation)
        val detailRecyclerView = view.findViewById<RecyclerView>(R.id.word_details)
        val errorContainer = view.findViewById<View>(R.id.error_container)
        val retryButton = errorContainer.findViewById<View>(R.id.retry_button)
        val loadingIndicator = view.findViewById<View>(R.id.loading_indicator)

        val headerSceneDependencies = object : WordDetailHeaderScene.Dependencies {

            override val image: ImageView = wordImageView
            override val title: TextView = wordTitleTextView
            override val translation: TextView = wordTranslationTextView

        }

        val detailSceneDependencies = object : WordDetailScene.Dependencies {

            override val recycler: RecyclerView = detailRecyclerView
            override val strings: WordDetailListStrings = stringsProvider
        }

        val loadingIndicatorSceneDependencies = object : LoadingIndicatorScene.Dependencies {

            override val indicator: View = loadingIndicator
        }

        val errorContainerDependencies = object : RetryRequestScene.Dependencies {

            override val retryContainer: View = errorContainer
            override val retryButton: View = retryButton
        }

        val headerScene = WordDetailHeaderScene(headerSceneDependencies)
        val detailScene = WordDetailScene(detailSceneDependencies)
        val loadingIndicatorScene = LoadingIndicatorScene(loadingIndicatorSceneDependencies)
        val errorContainerScene = RetryRequestScene(errorContainerDependencies)

        subscriptions += headerScene.bind(lifecycleStorage.headerViewModel)
        subscriptions += detailScene.bind(lifecycleStorage.detailsViewModel)
        subscriptions += loadingIndicatorScene.bind(lifecycleStorage.loadingIndicatorViewModel)
        subscriptions += errorContainerScene.bind(lifecycleStorage.errorViewModel)
    }

    override fun onDestroy() {
        subscriptions.dispose()
        super.onDestroy()
    }
}