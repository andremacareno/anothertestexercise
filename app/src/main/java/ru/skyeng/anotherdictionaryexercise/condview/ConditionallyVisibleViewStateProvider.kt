package ru.skyeng.anotherdictionaryexercise.condview

import io.reactivex.Observable

interface ConditionallyVisibleViewStateProvider {

    val visiblility: Observable<Boolean>
}