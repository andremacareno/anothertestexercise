package ru.skyeng.anotherdictionaryexercise.condview.ui

import android.view.View
import androidx.core.view.isVisible
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import ru.skyeng.anotherdictionaryexercise.condview.ConditionallyVisibleViewModel
import ru.skyeng.anotherdictionaryexercise.ui.AbsScene

class ConditionallyVisibleScene(private val dependencies: Dependencies) : AbsScene<ConditionallyVisibleViewModel>() {

    interface Dependencies {

        val conditionallyVisibleView: View
    }

    override fun onBind(vm: ConditionallyVisibleViewModel): Disposable {
        return vm.visible.observeOn(AndroidSchedulers.mainThread()).subscribe { dependencies.conditionallyVisibleView.isVisible = it }
    }
}