package ru.skyeng.anotherdictionaryexercise.condview

import io.reactivex.Observable

interface ConditionallyVisibleViewModel {

    val visible: Observable<Boolean>

    companion object {

        fun createDefault(stateProvider: ConditionallyVisibleViewStateProvider): ConditionallyVisibleViewModel = Impl(stateProvider)
    }

    private class Impl(stateProvider: ConditionallyVisibleViewStateProvider) : ConditionallyVisibleViewModel {

        override val visible: Observable<Boolean> = stateProvider.visiblility
    }
}