package ru.skyeng.anotherdictionaryexercise.loadingindicator.ui

import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import ru.skyeng.anotherdictionaryexercise.loadingindicator.LoadingIndicatorViewModel
import ru.skyeng.anotherdictionaryexercise.ui.AbsScene

class LoadingIndicatorScene(private val dependencies: Dependencies) : AbsScene<LoadingIndicatorViewModel>() {

    interface Dependencies {

        val indicator: View
    }

    override fun onBind(vm: LoadingIndicatorViewModel): Disposable {
        return vm.loading.observeOn(AndroidSchedulers.mainThread()).subscribe { dependencies.indicator.visibility = if (it) View.VISIBLE else View.INVISIBLE }
    }
}