package ru.skyeng.anotherdictionaryexercise.loadingindicator

import io.reactivex.Observable

interface LoadingIndicatorViewModel {

    val loading: Observable<Boolean>

    companion object {

        fun createDefault(stateProvider: LoadingStateProvider): LoadingIndicatorViewModel = Impl(stateProvider)
    }

    private class Impl(provider: LoadingStateProvider) : LoadingIndicatorViewModel {

        override val loading: Observable<Boolean> = provider.loading
    }
}