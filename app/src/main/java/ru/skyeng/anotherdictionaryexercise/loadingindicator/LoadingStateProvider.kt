package ru.skyeng.anotherdictionaryexercise.loadingindicator

import io.reactivex.Observable

interface LoadingStateProvider {

    val loading: Observable<Boolean>
}