package ru.skyeng.anotherdictionaryexercise.routing

import ru.skyeng.anotherdictionaryexercise.models.Word

interface SearchFlowRouter {

    fun start()
    fun openWordDetail(word: Word)
    fun goBack()
}