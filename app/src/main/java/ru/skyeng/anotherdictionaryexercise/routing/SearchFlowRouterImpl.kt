package ru.skyeng.anotherdictionaryexercise.routing

import androidx.fragment.app.FragmentManager
import ru.skyeng.anotherdictionaryexercise.fragments.SearchFragment
import ru.skyeng.anotherdictionaryexercise.fragments.WordDetailFragment
import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.utils.PostponeUntilResumeExecutor

class SearchFlowRouterImpl(
    private val containerId: Int,
    private val fragmentManager: FragmentManager,
    private val postponeUntilResumeExecutor: PostponeUntilResumeExecutor
) : SearchFlowRouter {

    override fun start() {
        postponeUntilResumeExecutor.postponeUntilResume {
            fragmentManager
                .beginTransaction()
                .add(containerId, SearchFragment::class.java, null)
                .setReorderingAllowed(true)
                .commit()
        }
    }

    override fun openWordDetail(word: Word) {
        postponeUntilResumeExecutor.postponeUntilResume {
            fragmentManager
                .beginTransaction()
                .replace(containerId, WordDetailFragment::class.java, null)
                .addToBackStack(null)
                .setReorderingAllowed(true)
                .commit()
        }
    }

    override fun goBack() {
        postponeUntilResumeExecutor.postponeUntilResume {
            fragmentManager.popBackStack()
        }
    }
}