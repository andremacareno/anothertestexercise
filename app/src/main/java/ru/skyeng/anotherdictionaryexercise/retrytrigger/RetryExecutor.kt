package ru.skyeng.anotherdictionaryexercise.retrytrigger

interface RetryExecutor {

    fun retry()
}