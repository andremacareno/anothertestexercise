package ru.skyeng.anotherdictionaryexercise.retrytrigger

import io.reactivex.Observable

interface ErrorStateProvider {

    val error: Observable<Boolean>
}