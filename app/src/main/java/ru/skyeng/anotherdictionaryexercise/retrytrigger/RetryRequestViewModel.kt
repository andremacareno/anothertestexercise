package ru.skyeng.anotherdictionaryexercise.retrytrigger

import io.reactivex.Observable

interface RetryRequestViewModel {

    val display: Observable<Boolean>
    fun retryRequested()

    companion object {

        fun createDefault(
            errorStateProvider: ErrorStateProvider,
            executor: RetryExecutor
        ): RetryRequestViewModel = Impl(errorStateProvider, executor)
    }

    private class Impl(
        private val errorStateProvider: ErrorStateProvider,
        private val executor: RetryExecutor
    ) : RetryRequestViewModel {

        override val display: Observable<Boolean> = errorStateProvider.error
        override fun retryRequested() = executor.retry()
    }
}