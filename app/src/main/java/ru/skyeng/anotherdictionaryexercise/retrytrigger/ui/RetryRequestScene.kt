package ru.skyeng.anotherdictionaryexercise.retrytrigger.ui

import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import ru.skyeng.anotherdictionaryexercise.retrytrigger.RetryRequestViewModel
import ru.skyeng.anotherdictionaryexercise.ui.AbsScene
import ru.skyeng.anotherdictionaryexercise.utils.bindClickListener

class RetryRequestScene(private val dependencies: Dependencies) : AbsScene<RetryRequestViewModel>() {

    interface Dependencies {

        val retryContainer: View
        val retryButton: View
    }

    override fun onBind(vm: RetryRequestViewModel): Disposable {
        val ret = CompositeDisposable()
        ret += vm.display.observeOn(AndroidSchedulers.mainThread()).subscribe { dependencies.retryContainer.visibility = if (it) View.VISIBLE else View.GONE }
        ret += dependencies.retryButton.bindClickListener { vm.retryRequested() }
        return ret
    }
}