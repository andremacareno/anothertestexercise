package ru.skyeng.anotherdictionaryexercise.api

data class UsageExampleApiModel(
    val text: String
)