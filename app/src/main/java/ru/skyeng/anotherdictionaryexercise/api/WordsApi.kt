package ru.skyeng.anotherdictionaryexercise.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WordsApi {

    @GET("api/public/v1/words/search")
    fun searchWords(@Query("search") query: String): Call<List<WordApiModel>>
}