package ru.skyeng.anotherdictionaryexercise.api

data class DefinitionApiModel(
    val text: String
)