package ru.skyeng.anotherdictionaryexercise.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MeaningsApi {

    @GET("api/public/v1/meanings")
    fun getMeaning(@Query("ids") id: Long): Call<List<MeaningApiModel>>
}