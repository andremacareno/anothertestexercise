package ru.skyeng.anotherdictionaryexercise.api

data class TranslationApiModel(
    val text: String,
    val note: String?
)