package ru.skyeng.anotherdictionaryexercise.api

data class WordApiModel(
    val id: Int,
    val text: String,
    val meanings: List<Meaning2ApiModel>
)