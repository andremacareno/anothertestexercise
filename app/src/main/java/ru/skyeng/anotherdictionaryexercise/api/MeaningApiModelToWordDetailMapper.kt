package ru.skyeng.anotherdictionaryexercise.api

import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.models.WordDetail
import ru.skyeng.anotherdictionaryexercise.models.WordUsageExample

object MeaningApiModelToWordDetailMapper {

    private val missingProtocolRegexp = Regex("^//")

    fun convert(from: MeaningApiModel): WordDetail {
        val img = from.images.firstOrNull()?.url ?: ""
        val fixedImg = (if (img.contains(missingProtocolRegexp)) "https:" else "") + img
        return WordDetail(
            word = Word(id = from.id.toLong(), origin = from.text, translation = from.translation.text, thumbnail = fixedImg),
            image = fixedImg,
            transcription = from.transcription,
            definition = from.definition.text,
            examples = from.examples.map { WordUsageExample(it.text) }
        )
    }
}