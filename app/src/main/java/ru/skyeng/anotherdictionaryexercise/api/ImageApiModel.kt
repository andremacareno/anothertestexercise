package ru.skyeng.anotherdictionaryexercise.api

data class ImageApiModel(
    val url: String
)