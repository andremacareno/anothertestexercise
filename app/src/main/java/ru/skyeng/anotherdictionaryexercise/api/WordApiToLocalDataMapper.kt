package ru.skyeng.anotherdictionaryexercise.api

import ru.skyeng.anotherdictionaryexercise.models.Word

object WordApiToLocalDataMapper {

    private val missingProtocolRegexp = Regex("^//")

    fun convert(from: WordApiModel): List<Word> {
        val origin = from.text
        return from.meanings.map {
            val id = it.id
            val translation = it.translation.text
            val thumbnail = (if (it.previewUrl.contains(missingProtocolRegexp)) "https:" else "") + it.previewUrl
            val photo = (if (it.imageUrl.contains(missingProtocolRegexp)) "https:" else "") + it.imageUrl
            Word(id.toLong(), origin, translation, thumbnail, photo)
        }
    }
}