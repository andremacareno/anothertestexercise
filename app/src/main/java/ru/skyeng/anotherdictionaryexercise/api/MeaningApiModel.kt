package ru.skyeng.anotherdictionaryexercise.api

data class MeaningApiModel(
    val id: String,
    val wordId: Int,
    val partOfSpeechCode: String,
    val text: String,
    val transcription: String,
    val translation: TranslationApiModel,
    val images: List<ImageApiModel>,
    val definition: DefinitionApiModel,
    val examples: List<UsageExampleApiModel>
)