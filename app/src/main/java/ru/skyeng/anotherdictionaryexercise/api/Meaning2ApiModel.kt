package ru.skyeng.anotherdictionaryexercise.api

data class Meaning2ApiModel(
    val id: Int,
    val partOfSpeechCode: String,
    val translation: TranslationApiModel,
    val previewUrl: String,
    val imageUrl: String,
    val transcription: String
)