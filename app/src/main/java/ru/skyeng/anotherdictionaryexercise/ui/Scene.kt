package ru.skyeng.anotherdictionaryexercise.ui

import io.reactivex.disposables.Disposable

interface Scene<ViewModel> {

    fun bind(vm: ViewModel): Disposable
}