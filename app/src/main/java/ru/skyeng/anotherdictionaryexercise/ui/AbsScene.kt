package ru.skyeng.anotherdictionaryexercise.ui

import io.reactivex.disposables.Disposable
import io.reactivex.disposables.SerialDisposable

abstract class AbsScene<ViewModel> : Scene<ViewModel>,
    Disposable {

    private val disposable = SerialDisposable()
    final override fun dispose() {
        onUnbind()
        disposable.dispose()
    }

    final override fun isDisposed(): Boolean {
        return disposable.isDisposed
    }

    final override fun bind(vm: ViewModel): Disposable {
        disposable.set(onBind(vm))
        return disposable
    }

    protected abstract fun onBind(vm: ViewModel): Disposable

    protected open fun onUnbind() {

    }
}