package ru.skyeng.anotherdictionaryexercise.ui

import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables

object EmptyScene : Scene<Any> {

    override fun bind(vm: Any): Disposable {
        return Disposables.empty()
    }
}