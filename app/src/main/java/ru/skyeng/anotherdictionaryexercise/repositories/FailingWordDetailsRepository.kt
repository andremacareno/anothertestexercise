package ru.skyeng.anotherdictionaryexercise.repositories

import io.reactivex.Single
import ru.skyeng.anotherdictionaryexercise.models.WordDetail
import java.util.concurrent.TimeUnit

class FailingWordDetailsRepository : WordDetailsRepository {

    override fun getDetails(id: Long): Single<WordDetail> {
        return Single.defer {
            Single.just(Unit)
                .delay(500, TimeUnit.MILLISECONDS)
                .flatMap { Single.error<WordDetail>(Exception("Test exception")) }
        }
    }
}