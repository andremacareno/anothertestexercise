package ru.skyeng.anotherdictionaryexercise.repositories

import io.reactivex.Single
import ru.skyeng.anotherdictionaryexercise.models.Word

interface WordsRepository {

    fun getWords(word: String): Single<List<Word>>
}