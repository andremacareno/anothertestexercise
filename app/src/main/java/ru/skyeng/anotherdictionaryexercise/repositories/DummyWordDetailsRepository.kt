package ru.skyeng.anotherdictionaryexercise.repositories

import io.reactivex.Single
import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.models.WordDetail

class DummyWordDetailsRepository : WordDetailsRepository {

    private val detail = WordDetail(
        word = Word(1L, "word", "слово", "https://d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=96&h=72"),
        image = "https://d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=640&h=480",
        transcription = "wɜːd",
        definition = "A single group of letters that are used together with a particular meaning.",
        examples = emptyList()
    )

    override fun getDetails(id: Long): Single<WordDetail> {
        return Single.defer { Single.just(detail) }
    }
}