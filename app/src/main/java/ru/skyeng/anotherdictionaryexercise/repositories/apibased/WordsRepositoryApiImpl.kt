package ru.skyeng.anotherdictionaryexercise.repositories.apibased

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import ru.skyeng.anotherdictionaryexercise.api.WordApiToLocalDataMapper
import ru.skyeng.anotherdictionaryexercise.api.WordsApi
import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.repositories.WordsRepository

class WordsRepositoryApiImpl(private val wordsApi: WordsApi) : WordsRepository {

    override fun getWords(word: String): Single<List<Word>> {
        return Single.defer {
            Single.fromCallable { wordsApi.searchWords(word).execute() }
                .subscribeOn(Schedulers.io())
                .map { it.body()!!
                    .map { WordApiToLocalDataMapper.convert(it) }
                    .fold(mutableListOf<Word>()) { a, b -> a.apply { a.addAll(b) } }
                }
        }
    }
}