package ru.skyeng.anotherdictionaryexercise.repositories

import io.reactivex.Single
import ru.skyeng.anotherdictionaryexercise.models.WordDetail

interface WordDetailsRepository {

    fun getDetails(id: Long): Single<WordDetail>
}