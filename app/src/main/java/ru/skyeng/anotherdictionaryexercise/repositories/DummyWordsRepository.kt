package ru.skyeng.anotherdictionaryexercise.repositories

import io.reactivex.Single
import ru.skyeng.anotherdictionaryexercise.models.Word
import java.util.concurrent.TimeUnit

class DummyWordsRepository : WordsRepository {

    private val word1 = Word(1L, "word", "слово", "https://d2zkmv5t5kao9.cloudfront.net/images/ea4f7ce85e73e44cec430fe87db3f3ce.jpeg?w=96&h=72")
    private val word2 = Word(2L, "word", "известие", "https://d2zkmv5t5kao9.cloudfront.net/images/d6e722ff3a3d143c40aad0bdb511c121.jpeg?w=96&h=72")
    private val word3 = Word(3L, "word", "разговор", "https://d2zkmv5t5kao9.cloudfront.net/images/04871e9722ec2653dbf4880554ba1ffe.jpeg?w=96&h=72")

    private val response = listOf(word1, word2, word3)

    override fun getWords(word: String): Single<List<Word>> {
        return Single.defer {
            if (word == "error") Single.just(Unit).delay(200, TimeUnit.MILLISECONDS).flatMap { Single.error<List<Word>>(IllegalArgumentException("test error")) }
            else Single.just(response)
        }
    }
}