package ru.skyeng.anotherdictionaryexercise.repositories.apibased

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import ru.skyeng.anotherdictionaryexercise.api.MeaningApiModelToWordDetailMapper
import ru.skyeng.anotherdictionaryexercise.api.MeaningsApi
import ru.skyeng.anotherdictionaryexercise.models.WordDetail
import ru.skyeng.anotherdictionaryexercise.repositories.WordDetailsRepository

class WordDetailsRepositoryApiImpl(private val meaningsApi: MeaningsApi) : WordDetailsRepository {

    override fun getDetails(id: Long): Single<WordDetail> {
        return Single.defer {
            Single
                .fromCallable { meaningsApi.getMeaning(id).execute() }
                .subscribeOn(Schedulers.io())
                .map { MeaningApiModelToWordDetailMapper.convert(it.body()!!.first()) }
        }
    }
}