package ru.skyeng.anotherdictionaryexercise.searchresults

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.wordselect.WordSelector

interface SearchResultsListViewModel {

    val data: Observable<List<Word>>

    fun selectWord(word: Word)

    companion object {

        fun createDefault(
            resultsProvider: SearchResultsProvider,
            wordSelector: WordSelector
        ): SearchResultsListViewModel = Impl(resultsProvider, wordSelector)
    }

    private class Impl(
        resultsProvider: SearchResultsProvider,
        private val wordSelector: WordSelector
    ) : SearchResultsListViewModel {

        override val data: Observable<List<Word>> = resultsProvider.searchResults

        override fun selectWord(word: Word) = wordSelector.selectWord(word)
    }
}