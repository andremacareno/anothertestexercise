package ru.skyeng.anotherdictionaryexercise.searchresults.ui

import androidx.recyclerview.widget.DiffUtil
import ru.skyeng.anotherdictionaryexercise.models.Word

class SearchResultsDiffCalculator(
    private val oldData: List<Word>,
    private val newData: List<Word>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldData.size
    }

    override fun getNewListSize(): Int {
        return newData.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldData[oldItemPosition]
        val newItem = newData[newItemPosition]
        return oldItem.id == newItem.id && oldItem.origin == newItem.origin
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldData[oldItemPosition]
        val newItem = newData[newItemPosition]
        return oldItem.id == newItem.id
    }


}