package ru.skyeng.anotherdictionaryexercise.searchresults.ui

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.searchresults.SearchResultsListViewModel
import ru.skyeng.anotherdictionaryexercise.ui.AbsScene
import ru.skyeng.anotherdictionaryexercise.utils.bindAdapter

class SearchResultsScene(private val dependencies: Dependencies) : AbsScene<SearchResultsListViewModel>() {

    interface Dependencies {

        val recycler: RecyclerView
        val itemSelectionHandler: (Word) -> Unit
    }

    private val currentListState = mutableListOf<Word>()
    private lateinit var adapter: SearchResultsAdapter

    override fun onBind(vm: SearchResultsListViewModel): Disposable {
        adapter = SearchResultsAdapter(currentListState) {
            vm.selectWord(it)
            dependencies.itemSelectionHandler(it)
        }
        val ret = CompositeDisposable()
        ret += dependencies.recycler.bindAdapter(adapter)
        ret += vm.data.observeOn(AndroidSchedulers.mainThread()).subscribe(::updateData)
        return ret
    }

    private fun updateData(newData: List<Word>) {
        val oldData = currentListState.toList()
        val diffCalculator = SearchResultsDiffCalculator(oldData, newData)
        val result = DiffUtil.calculateDiff(diffCalculator)
        currentListState.clear()
        currentListState.addAll(newData)
        result.dispatchUpdatesTo(adapter)
    }
}