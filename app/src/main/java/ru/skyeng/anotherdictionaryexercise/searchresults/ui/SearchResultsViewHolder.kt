package ru.skyeng.anotherdictionaryexercise.searchresults.ui

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.skyeng.anotherdictionaryexercise.R
import ru.skyeng.anotherdictionaryexercise.models.Word

class SearchResultsViewHolder(
    itemView: View,
    itemSelectionHandler: (Word) -> Unit
) : RecyclerView.ViewHolder(itemView) {

    private val thumbnail: ImageView = itemView.findViewById(R.id.thumbnail)
    private val word: TextView = itemView.findViewById(R.id.word)
    private val translation: TextView = itemView.findViewById(R.id.translation)

    private lateinit var item: Word

    fun bind(word: Word) {
        Glide.with(itemView).load(word.thumbnail).circleCrop().into(thumbnail)
        this.word.text = word.origin
        translation.text = word.translation
        item = word
    }

    init {
        itemView.setOnClickListener { itemSelectionHandler.invoke(item) }
    }
}