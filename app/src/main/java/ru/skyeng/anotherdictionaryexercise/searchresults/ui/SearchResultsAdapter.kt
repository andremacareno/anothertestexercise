package ru.skyeng.anotherdictionaryexercise.searchresults.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.skyeng.anotherdictionaryexercise.R
import ru.skyeng.anotherdictionaryexercise.models.Word

class SearchResultsAdapter(
    private val data: List<Word>,
    private val itemSelectionHandler: (Word) -> Unit
) : RecyclerView.Adapter<SearchResultsViewHolder>() {

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search_result_entry, parent, false)
        return SearchResultsViewHolder(view, itemSelectionHandler)
    }

    override fun onBindViewHolder(holder: SearchResultsViewHolder, position: Int) {
        val word = data[position]
        holder.bind(word)
    }
}