package ru.skyeng.anotherdictionaryexercise.searchresults

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.models.Word

interface SearchResultsProvider {

    val searchResults: Observable<List<Word>>
}