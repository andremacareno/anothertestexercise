package ru.skyeng.anotherdictionaryexercise.json

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object JsonMapper {

    private val gson = Gson()

    fun <T> deserialize(json: String, type: Class<T>): T {
        return gson.fromJson(json, type)
    }

    fun <T> deserializeList(json: String): List<T> {
        return gson.fromJson(json, object : TypeToken<List<T>>() {}.type)
    }

    inline fun <reified T> deserialize(json: String): T {
        return deserialize(json, T::class.java)
    }
}