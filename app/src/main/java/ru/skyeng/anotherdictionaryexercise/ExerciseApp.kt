package ru.skyeng.anotherdictionaryexercise

import android.app.Application
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import ru.skyeng.anotherdictionaryexercise.api.MeaningsApi
import ru.skyeng.anotherdictionaryexercise.api.WordsApi
import ru.skyeng.anotherdictionaryexercise.repositories.WordDetailsRepository
import ru.skyeng.anotherdictionaryexercise.repositories.WordsRepository
import ru.skyeng.anotherdictionaryexercise.repositories.apibased.WordDetailsRepositoryApiImpl
import ru.skyeng.anotherdictionaryexercise.repositories.apibased.WordsRepositoryApiImpl
import ru.skyeng.anotherdictionaryexercise.servicelocator.ServiceLocator
import ru.skyeng.anotherdictionaryexercise.servicelocator.ServiceLocatorImpl
import ru.skyeng.anotherdictionaryexercise.servicelocator.get

class ExerciseApp : Application() {

    private lateinit var retrofit: Retrofit
    lateinit var serviceLocator: ServiceLocator
        private set

    override fun onCreate() {
        super.onCreate()
        sharedInstance = this
        initRetrofit()
        initServiceLocator()
    }

    private fun initRetrofit() {
        retrofit = Retrofit.Builder().baseUrl("https://dictionary.skyeng.ru/").addConverterFactory(GsonConverterFactory.create()).build()
    }

    private fun initServiceLocator() {
        serviceLocator = ServiceLocatorImpl(
            mapOf(
                WordsApi::class.java to lazy { retrofit.create<WordsApi>() },
                MeaningsApi::class.java to lazy { retrofit.create<MeaningsApi>() },
                WordsRepository::class.java to lazy { WordsRepositoryApiImpl(serviceLocator.get()) },
                WordDetailsRepository::class.java to lazy { WordDetailsRepositoryApiImpl(serviceLocator.get()) }
            )
        )
    }

    companion object {

        @JvmStatic lateinit var sharedInstance: ExerciseApp
            private set
    }
}