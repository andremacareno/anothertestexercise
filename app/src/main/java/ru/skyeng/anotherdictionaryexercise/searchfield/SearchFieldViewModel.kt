package ru.skyeng.anotherdictionaryexercise.searchfield

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable

interface SearchFieldViewModel {

    val displayClearButton: Observable<Boolean>
    val clearSearchField: Observable<Unit>

    fun search(keyword: String)
    fun clearSearchFieldRequested()

    companion object {

        fun createDefault(inputReceiver: SearchInputReceiver): SearchFieldViewModel = Impl(inputReceiver)
    }

    private class Impl(private val inputReceiver: SearchInputReceiver) : SearchFieldViewModel {

        private val text = BehaviorRelay.createDefault("")
        override val displayClearButton: Observable<Boolean> = text.map { it.isNotEmpty() }.distinctUntilChanged()
        override val clearSearchField = PublishRelay.create<Unit>()

        override fun search(keyword: String) = inputReceiver.search(keyword).apply { text.accept(keyword) }
        override fun clearSearchFieldRequested() = clearSearchField.accept(Unit)
    }
}