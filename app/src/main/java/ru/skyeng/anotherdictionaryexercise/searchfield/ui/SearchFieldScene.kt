package ru.skyeng.anotherdictionaryexercise.searchfield.ui

import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import ru.skyeng.anotherdictionaryexercise.searchfield.SearchFieldViewModel
import ru.skyeng.anotherdictionaryexercise.ui.AbsScene
import ru.skyeng.anotherdictionaryexercise.ui.BackPressHandlingEditText
import ru.skyeng.anotherdictionaryexercise.utils.bindAfterTextChanged
import ru.skyeng.anotherdictionaryexercise.utils.bindBackPressedHandler
import ru.skyeng.anotherdictionaryexercise.utils.bindClickListener

class SearchFieldScene(private val dependencies: Dependencies) : AbsScene<SearchFieldViewModel>() {

    interface Dependencies {

        val searchField: BackPressHandlingEditText
        val clear: View
    }

    override fun onBind(vm: SearchFieldViewModel): Disposable {
        val ret = CompositeDisposable()
        ret += vm.displayClearButton.observeOn(AndroidSchedulers.mainThread()).subscribe { dependencies.clear.visibility = if (it) View.VISIBLE else View.INVISIBLE }
        ret += vm.clearSearchField.observeOn(AndroidSchedulers.mainThread()).subscribe { dependencies.searchField.text = null }
        ret += dependencies.clear.bindClickListener { vm.clearSearchFieldRequested() }
        ret += dependencies.searchField.bindAfterTextChanged { vm.search(it.toString()) }
        ret += dependencies.searchField.bindBackPressedHandler { vm.clearSearchFieldRequested() }
        return ret
    }

}