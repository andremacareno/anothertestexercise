package ru.skyeng.anotherdictionaryexercise.searchfield

interface SearchInputReceiver {

    fun search(keyword: String)
}