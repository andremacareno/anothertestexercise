package ru.skyeng.anotherdictionaryexercise.models

data class WordDetail(
    val word: Word,
    val image: String = "",
    val transcription: String = "",
    val definition: String = "",
    val examples: List<WordUsageExample> = emptyList()
)