package ru.skyeng.anotherdictionaryexercise.models

data class WordUsageExample(
    val text: String
)