package ru.skyeng.anotherdictionaryexercise.models

data class Word(
    val id: Long,
    val origin: String,
    val translation: String,
    val thumbnail: String,
    val photo: String = ""
)