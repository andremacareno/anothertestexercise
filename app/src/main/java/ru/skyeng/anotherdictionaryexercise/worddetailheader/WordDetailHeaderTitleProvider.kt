package ru.skyeng.anotherdictionaryexercise.worddetailheader

import io.reactivex.Observable

interface WordDetailHeaderTitleProvider {

    val title: Observable<String>
    val translation: Observable<String>
}