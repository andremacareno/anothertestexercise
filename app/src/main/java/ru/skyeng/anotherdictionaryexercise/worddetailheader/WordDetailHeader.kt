package ru.skyeng.anotherdictionaryexercise.worddetailheader

data class WordDetailHeader(
    val title: String,
    val image: String,
    val translation: String
)