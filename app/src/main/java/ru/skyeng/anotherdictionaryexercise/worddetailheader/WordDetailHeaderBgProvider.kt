package ru.skyeng.anotherdictionaryexercise.worddetailheader

import io.reactivex.Observable

interface WordDetailHeaderBgProvider {

    val backgroundImage: Observable<String>
}