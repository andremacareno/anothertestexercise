package ru.skyeng.anotherdictionaryexercise.worddetailheader

import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import ru.skyeng.anotherdictionaryexercise.ui.AbsScene

class WordDetailHeaderScene(private val dependencies: Dependencies) : AbsScene<WordDetailHeaderViewModel>() {

    interface Dependencies {

        val image: ImageView
        val title: TextView
        val translation: TextView
    }

    override fun onBind(vm: WordDetailHeaderViewModel): Disposable {
        return vm.header.observeOn(AndroidSchedulers.mainThread()).subscribe {
            Glide.with(dependencies.image).load(it.image).into(dependencies.image)
            dependencies.title.text = it.title
            dependencies.translation.text = it.translation
        }
    }
}