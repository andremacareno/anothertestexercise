package ru.skyeng.anotherdictionaryexercise.worddetailheader

import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable

interface WordDetailHeaderViewModel {

    val header: Observable<WordDetailHeader>

    companion object {

        fun createDefault(
            titleProvider: WordDetailHeaderTitleProvider,
            bgImageProvider: WordDetailHeaderBgProvider
        ): WordDetailHeaderViewModel = Impl(titleProvider, bgImageProvider)
    }

    private class Impl(
        titleProvider: WordDetailHeaderTitleProvider,
        bgImageProvider: WordDetailHeaderBgProvider
    ): WordDetailHeaderViewModel {

        override val header: Observable<WordDetailHeader> = BehaviorRelay.create<WordDetailHeader>().apply {
            Observable
                .merge(
                    titleProvider.title.map(Update::UpdateTitle),
                    bgImageProvider.backgroundImage.map(Update::UpdateBackgroundImage),
                    titleProvider.translation.map(Update::UpdateTranslation)
                )
                .scan(WordDetailHeader("", "", "")) { state, update -> update.applyUpdate(state) }
                .subscribe(this)
        }

        private sealed class Update {

            abstract fun applyUpdate(state: WordDetailHeader): WordDetailHeader

            class UpdateTitle(private val newTitle: String) : Update() {

                override fun applyUpdate(state: WordDetailHeader): WordDetailHeader = state.copy(title = newTitle)
            }

            class UpdateBackgroundImage(private val newImage: String) : Update() {

                override fun applyUpdate(state: WordDetailHeader): WordDetailHeader = state.copy(image = newImage)
            }

            class UpdateTranslation(private val newTranslation: String) : Update() {

                override fun applyUpdate(state: WordDetailHeader): WordDetailHeader = state.copy(translation = newTranslation)
            }
        }
    }
}