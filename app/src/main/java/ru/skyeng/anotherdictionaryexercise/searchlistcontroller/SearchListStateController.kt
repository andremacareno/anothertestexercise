package ru.skyeng.anotherdictionaryexercise.searchlistcontroller

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.schedulers.Schedulers
import ru.skyeng.anotherdictionaryexercise.repositories.WordsRepository
import java.util.concurrent.TimeUnit

interface SearchListStateController {

    val listState: Observable<SearchListState>

    fun searchFor(keyword: String)
    fun retryLastSearch()

    companion object {

        fun createDefault(
            wordsRepository: WordsRepository,
            searchDelayScheduler: Scheduler = Schedulers.computation()
        ) : SearchListStateController = Impl(wordsRepository, searchDelayScheduler)
    }

    private class Impl(
        wordsRepository: WordsRepository,
        searchDelayScheduler: Scheduler
    ) : SearchListStateController {

        private val _keyword = BehaviorRelay.create<String>()
        private val trimKeyword = _keyword.map { it.trim() }
        private val retry = PublishRelay.create<Unit>()

        private val keyword = Observable.merge(
            trimKeyword.debounce(300, TimeUnit.MILLISECONDS, searchDelayScheduler).distinctUntilChanged(),
            retry.withLatestFrom(trimKeyword) { _, keyword -> keyword }
        ).filter { it.isNotEmpty() }

        private val searchBegan = keyword.map(SearchListStateEffect::Searching)

        private val data = keyword
            .switchMap {
                wordsRepository.getWords(it).toObservable()
                    .map { SearchListStateEffect.ResultsReceived(it) as SearchListStateEffect }
                    .onErrorReturn { SearchListStateEffect.Error }
            }

        private val resetSearchResults = trimKeyword
            .filter { it.isEmpty() }
            .map { SearchListStateEffect.ResetSearchResults }

        private val effect = Observable.merge(
            searchBegan,
            data,
            resetSearchResults
        )

        override val listState: Observable<SearchListState> = BehaviorRelay.create<SearchListState>().apply {
            effect
                .scan(SearchListState(emptyList(), false, false, "")) { state, effect -> effect.applyEffect(state) }
                .subscribe(this)
        }

        override fun searchFor(keyword: String) = _keyword.accept(keyword)

        override fun retryLastSearch() = retry.accept(Unit)

    }
}