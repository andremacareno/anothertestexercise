package ru.skyeng.anotherdictionaryexercise.searchlistcontroller

import ru.skyeng.anotherdictionaryexercise.models.Word

data class SearchListState(
    val data: List<Word>,
    val loading: Boolean,
    val error: Boolean,
    val query: String
)