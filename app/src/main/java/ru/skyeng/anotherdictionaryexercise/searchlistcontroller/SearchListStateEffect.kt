package ru.skyeng.anotherdictionaryexercise.searchlistcontroller

import ru.skyeng.anotherdictionaryexercise.models.Word

sealed class SearchListStateEffect {

    abstract fun applyEffect(state: SearchListState): SearchListState

    class Searching(private val query: String) : SearchListStateEffect() {

        override fun applyEffect(state: SearchListState): SearchListState {
            return state.copy(query = query, loading = true, error = false)
        }
    }

    object Error : SearchListStateEffect() {

        override fun applyEffect(state: SearchListState): SearchListState {
            return state.copy(loading = false, error = true, data = emptyList())
        }
    }

    class ResultsReceived(val data: List<Word>) : SearchListStateEffect() {

        override fun applyEffect(state: SearchListState): SearchListState {
            return state.copy(loading = false, error = false, data = data)
        }
    }

    object ResetSearchResults : SearchListStateEffect() {

        override fun applyEffect(state: SearchListState): SearchListState = state.copy(query = "", data = emptyList(), loading = false, error = false)
    }
}