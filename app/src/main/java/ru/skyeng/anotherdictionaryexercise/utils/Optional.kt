package ru.skyeng.anotherdictionaryexercise.utils

class Optional<out T> private constructor(private val value: T?) {

    val isPresent: Boolean = value != null

    fun request(): T? = value
    fun require(): T = value!!

    companion object {

        private val EMPTY = Optional(null)

        fun <T> of(value: T?): Optional<T> = if (value == null) EMPTY else Optional(value)
        fun <T> empty(): Optional<T> = EMPTY
    }
}