package ru.skyeng.anotherdictionaryexercise.utils

class PostponeUntilResumeExecutor() {

    var resumed = false
    var taskToExecute: (() -> Unit)? = null

    fun notifyPaused() {
        resumed = false
    }

    fun notifyResumed() {
        resumed = true
        taskToExecute?.invoke()
        taskToExecute = null
    }

    fun postponeUntilResume(task: () -> Unit) {
        if (resumed) {
            task.invoke()
        } else {
            taskToExecute = task
        }
    }
}