package ru.skyeng.anotherdictionaryexercise.utils

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.disposables.Disposable
import ru.skyeng.anotherdictionaryexercise.ui.BackPressHandlingEditText

inline fun TextView.bindAfterTextChanged(crossinline listener: (CharSequence) -> Unit) = object : Disposable {

    private val textWatcher: TextWatcher = object : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) = Unit
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) = Unit

        override fun afterTextChanged(s: Editable) = listener(s)

    }

    private var disposed = false
    override fun dispose() {
        removeTextChangedListener(textWatcher)
        disposed = true
    }

    override fun isDisposed(): Boolean {
        return disposed
    }

    init {
        addTextChangedListener(textWatcher)
    }
}

inline fun View.bindClickListener(crossinline listener: () -> Unit) = object : Disposable {

    private val clickListener = View.OnClickListener { listener.invoke() }

    private var disposed = false

    override fun dispose() {
        setOnClickListener(null)
        disposed = true
    }

    override fun isDisposed(): Boolean {
        return disposed
    }

    init {
        setOnClickListener(clickListener)
    }
}

fun RecyclerView.bindAdapter(adapter: RecyclerView.Adapter<*>) = object : Disposable {

    private var disposed = false

    override fun dispose() {
        setAdapter(null)
        disposed = false
    }

    override fun isDisposed(): Boolean {
        return disposed
    }

    init {
        setAdapter(adapter)
    }

}

fun BackPressHandlingEditText.bindBackPressedHandler(handler: () -> Unit) = object : Disposable {

    private var disposed = false

    override fun dispose() {
        disposed = true
        doOnBackPress(null)
    }

    override fun isDisposed(): Boolean {
        return disposed
    }

    init {
        doOnBackPress(handler)
    }
}