package ru.skyeng.anotherdictionaryexercise.utils

import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.getSystemService

fun View.hideKeyboard() {
    val inputManager = context.getSystemService<InputMethodManager>()!!
    inputManager.hideSoftInputFromWindow(windowToken, 0)
}