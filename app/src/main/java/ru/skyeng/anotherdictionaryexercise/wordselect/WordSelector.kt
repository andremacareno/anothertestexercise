package ru.skyeng.anotherdictionaryexercise.wordselect

import ru.skyeng.anotherdictionaryexercise.models.Word

interface WordSelector {

    fun selectWord(word: Word)
    fun deselectWord()
}