package ru.skyeng.anotherdictionaryexercise.wordselect

import io.reactivex.Observable
import ru.skyeng.anotherdictionaryexercise.models.Word
import ru.skyeng.anotherdictionaryexercise.utils.Optional

interface SelectedWordProvider {

    val selectedWord: Observable<Optional<Word>>
}